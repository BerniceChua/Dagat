﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardsManager1 : MonoBehaviour
{
    [SerializeField] GameObject m_cube;
    [SerializeField] GameObject m_sphere;
    [SerializeField] GameObject m_capsule;
    [SerializeField] GameObject m_cylinder;

    [SerializeField] private float[] lengthAndWidths;

    [SerializeField] private GameObject[] m_hazardsPrefabs;

    [SerializeField] private float m_randomStartingXMin = -2;
    [SerializeField] private float m_randomStartingXMax = 5;
    [SerializeField] private float m_randomYMin = -6;
    [SerializeField] private float m_randomYMax = 6;

    [SerializeField] private float m_randomOngoingXMin = -5;
    [SerializeField] private float m_randomOngoingXMax = 5;

    [SerializeField] public GameObject m_playerCharacter;
    private float m_xOffset;
    private float m_yOffset;

    
    private Vector2 m_screenBoundMin;
    private Vector2 m_screenBoundary;
    [SerializeField] private LayerMask m_layerMaskCollisionTest;
    [SerializeField] private float m_spacingPadding;


    private void Awake() {
        m_hazardsPrefabs = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++) {
            m_hazardsPrefabs[i] = transform.GetChild(i).gameObject;

            m_hazardsPrefabs[i].AddComponent<BecameInvisible>();
            Debug.Log("Added BecameInvisible component");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

        for (int i = 0; i < m_hazardsPrefabs.Length; i++) {
            /// Get a prefab (shark, jellyfish, plastic bag)
            /// Find out distance from each other
                /// For each Prefab add a width and height floating point array (SAME SIZE - length as your hazards)
                /// First we need to find the length and width of object
            /// Allow the code to make their distances closer

            //Extents2D thisExtent = new Extents2D();

            float xVal = Random.Range(m_screenBoundary.x + m_randomStartingXMin, m_screenBoundary.x + m_randomStartingXMax);
            float yVal = Random.Range(m_randomYMin, m_randomYMax);
            //Debug.Log("instantiating " + m_hazardsPrefabs[i].name + xVal);

            //var thisGameObj = Instantiate(m_hazardsPrefabs[i], new Vector2(xVal, yVal), Quaternion.Euler(0, 0, 0));

            //m_xOffset = 20.0f;
            //m_yOffset = 0.0f;
            m_xOffset = xVal;
            m_yOffset = yVal;

            //m_hazardsPrefabs[i].GetComponent<MarineLifeMovement>().m_PosOffset = new Vector2(xVal, yVal);

            /// Choose a position that doesn't overlap on the specified layermask
            Vector2 randomSpawnPosition = GetValidSpawnLocation(m_hazardsPrefabs[i]);
            if (randomSpawnPosition == Vector2.zero) {
                Debug.Log("no more room!");
                //Destroy(newObject);
            } else {
                /// For a cleaner hierachy, parent this game object to this hazards manager.
                //m_hazardsPrefabs[i].transform.SetParent(this.transform);
                /// no need for this anymore, since they are already pre-parented to the spawner.

                /// Reposition to the next offset.
                //hazard.transform.position = new Vector2(xVal, yVal);
                m_hazardsPrefabs[i].transform.position = randomSpawnPosition;
            }

            Debug.Log("m_hazardsPrefabs[i].transform.position = " + m_hazardsPrefabs[i].transform.position);
        }


    }

    // Update is called once per frame
    void Update() {

    }

    public void RecycleHazards(GameObject hazard) {
        //Debug.Log("Recycling " + hazard.name);

        /// Choose a position that doesn't overlap on the specified layermask
        Vector2 randomSpawnPosition = GetValidSpawnLocation(hazard);
        if (randomSpawnPosition == Vector2.zero) {
            Debug.Log("no more room!");
            //Destroy(newObject);
        } else {
            /// Reposition to the next offset.
            //hazard.transform.position = new Vector2(xVal, yVal);
            hazard.transform.position = randomSpawnPosition;
        }

        Debug.Log("hazard.transform.position = " + hazard.transform.position);
    }

    private Vector2 GetValidSpawnLocation(GameObject hazard) {
        /// Get Extents of this hazard
        SpriteRenderer spriteExtents = hazard.GetComponent<SpriteRenderer>();
        float rightExtent = hazard.GetComponent<MarineLifeMovement>().m_RightExtent;
        float leftExtent = hazard.GetComponent<MarineLifeMovement>().m_LeftExtent;
        float upExtent = hazard.GetComponent<MarineLifeMovement>().m_UpExtent;
        float downExtent = hazard.GetComponent<MarineLifeMovement>().m_DownExtent;

        float rightEdge = rightExtent - hazard.transform.position.x;
        float leftEdge = hazard.transform.position.x - leftExtent;
        float upEdge = upExtent - hazard.transform.position.y;
        float downEdge = hazard.transform.position.y - downExtent;

        /// Sets the new postion to (0,0,0). Changed to Vector 3 3 in my code because of the objectCollider.bounds.extents which is a Vector 3
        Vector2 newPosition = Vector2.zero;

        /// Boolian for getting out of the Do/While loop
        bool validPosition = false;

        /// failsafe to prevent infinite loops
        int failureLimit = 100;
        int fails = 0;

        /// screen boundary max is upper the right corner = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z)
        /// screen boundary min Vector3(0,0,0) is the lower left corner = Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z)
        //m_screenBoundMin = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f - Screen.height / 2, 0.0f));
        //m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 2, Screen.height + (Screen.height / 2), 0.0f));
        m_screenBoundMin = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f - (Screen.height / 2), 0.0f));
        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 2, Screen.height + (Screen.height / 2), 0.0f));
        
        do
        {
            float xVal = Random.Range(m_screenBoundary.x + m_randomOngoingXMin, m_screenBoundary.x + m_randomOngoingXMax);

            //Debug.Log("m_screenBoundary.x = " + m_screenBoundary.x);
            //Debug.Log("xVal = " + xVal);
            /// The minumum & maximum values came from 10.8 divided by 2.
            /// 10.8 came from the height of the intended play area.
            float yVal = Mathf.Clamp((Random.Range(m_playerCharacter.transform.position.y + m_yOffset + m_randomYMin, m_playerCharacter.transform.position.y + m_yOffset + m_randomYMax)), -52.0f, 52.0f);

            //Vector3 hazardPosition = new Vector3(spriteExtents.bounds.extents.x + m_spacingPadding, spriteExtents.bounds.extents.y + m_spacingPadding, 0.0f);
            Vector3 hazardPosition = new Vector3(xVal, yVal, 0.0f);

            /// we add and subtract the object collider's half-width (extents) from the new position
            /// to get the opposing corners of the square area that the object will take up
            /// at the new position
            //Vector3 min = ((newPosition) - (hazardPosition));
            //Vector3 max = ((newPosition) + (hazardPosition));
            //Vector2 min = ((newPosition) - (m_screenBoundary));
            //Vector2 max = ((newPosition) + (m_screenBoundary));
            Vector2 min = m_screenBoundMin;
            //Vector2 max = m_screenBoundary + new Vector2(m_screenBoundary.x * 2, m_screenBoundary.y);
            Vector2 max = m_screenBoundary;
            Debug.Log("min = " + min);
            Debug.Log("max = " + max);

            if ((newPosition.x <= (rightEdge + m_spacingPadding)) && (newPosition.x >= (leftEdge - m_spacingPadding)) && (newPosition.y >= (downEdge - m_spacingPadding)) && (newPosition.y <= (upEdge + m_spacingPadding)))
            {
                Debug.Log("Inside the first if statement");
                /// Checks for overlapping
                Collider2D[] overlapObjects = Physics2D.OverlapAreaAll(min, max, m_layerMaskCollisionTest);

                //if it does not overlap any objects in the layermask

                if (overlapObjects.Length == 0)
                {
                    Debug.Log("good");

                    //break out of the Do/While loop
                    validPosition = true;

                    // reset the failsafe
                    fails = 0;
                }
                else
                {
                    Debug.Log("Overlapping has occured");

                    fails++;
                }
            }
        } while (!validPosition && (fails < failureLimit));

        return newPosition;
    }

    private void OnDrawGizmos() {
        //Debug.DrawLine(new Vector3(Screen.width, 0.0f - (Screen.height / 2), 0.0f), Vector3.forward, Color.white);
        //Debug.DrawLine(new Vector3(0, Screen.height + (Screen.height / 2), 0.0f), Vector3.forward, Color.red);

        Debug.Log("m_screenBoundMin = " + m_screenBoundMin);
        //Instantiate(m_cube, m_screenBoundMin, Quaternion.identity);
        Gizmos.DrawWireSphere(m_screenBoundMin, 1);

        Debug.Log("m_screenBoundary = " + m_screenBoundary);
        //Instantiate(m_sphere, m_screenBoundary, Quaternion.identity);
        Gizmos.DrawWireSphere(m_screenBoundary, 1);

        Vector2 STWPscreenWidthScreenHeight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));
        Debug.Log("screentoworldpoint screen.width, screen.height = " + STWPscreenWidthScreenHeight);
        //Instantiate(m_capsule, STWPscreenWidthScreenHeight, Quaternion.identity);
        Gizmos.DrawWireSphere(STWPscreenWidthScreenHeight, 1);

        Vector2 STWP000 = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0.0f));
        Debug.Log("screentoworldpoint (0,0,0) = " + STWP000);
        //Instantiate(m_cylinder, STWP000, Quaternion.identity);
        Gizmos.DrawWireSphere(STWP000, 1);

    }

}

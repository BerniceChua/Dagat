﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardsManager3 : MonoBehaviour
{
    [SerializeField] private GameObject[] m_hazardsPrefabs;

    [SerializeField] private float m_randomStartingXMin = -2;
    [SerializeField] private float m_randomStartingXMax = 5;
    [SerializeField] private float m_randomYMin = -6;
    [SerializeField] private float m_randomYMax = 6;

    [SerializeField] private float m_randomOngoingXMin = -5;
    [SerializeField] private float m_randomOngoingXMax = 5;

    [SerializeField] public GameObject m_playerCharacter;
    private float m_xOffset;
    private float m_yOffset;

    public GameObject itemToSpread;

    public float m_ItemXSpread = 10.0f;
    public float m_ItemYSpread = 10.0f;
    public float m_ItemZSpread = 0.0f;

    private Vector2 m_screenBoundMin;
    private Vector2 m_screenBoundMax;

#region Originally from NoOverlap2D
    //public GameObject[] itemsToPickFrom;
    public float overlapTestBoxSize = 10.0f;
    public float m_OverlapTestBoxSizeMinimum = 1.0f;

    public LayerMask spawnedObjectLayer;

    /// <summary>
    /// Put this outside the function so it's not making a new array each time.
    /// </summary>
    private Collider2D[] m_collidersInsideOverlapBox = new Collider2D[1];
    private Vector3 m_overlapTestBoxScale;
    private Vector2 m_minV2;
    private Vector2 m_maxV2;
#endregion

    private void Awake() {
        m_hazardsPrefabs = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            m_hazardsPrefabs[i] = transform.GetChild(i).gameObject;

            m_hazardsPrefabs[i].AddComponent<BecameInvisible1>();
            Debug.Log("Added BecameInvisible component to " + m_hazardsPrefabs[i].name);
        }
    }

    // Start is called before the first frame update
    void Start() {
        m_screenBoundMax = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    // Update is called once per frame
    void Update() {
        //m_xOffset = m_playerCharacter.transform.position.x;
        //m_yOffset = m_playerCharacter.transform.position.y;
        //SpreadItem();
    }

    public void RecycleHazards(GameObject hazard) {
        //Debug.Log("Recycling " + hazard.name);

        m_screenBoundMax = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        float xVal = Random.Range(m_screenBoundMax.x + m_randomOngoingXMin, m_screenBoundMax.x + m_randomOngoingXMax);
        //Debug.Log("xVal = " + xVal);
        /// The minumum & maximum values came from 10.8 divided by 2.
        /// 10.8 came from the height of the intended play area.
        float yVal = Mathf.Clamp((Random.Range(m_playerCharacter.transform.position.y + m_yOffset + m_randomYMin, m_playerCharacter.transform.position.y + m_yOffset + m_randomYMax)), -52.0f, 52.0f);
        //float yVal = Mathf.Clamp((Random.Range(-m_screenBoundary.y + m_randomYMin, m_screenBoundary.y + m_randomYMax)), -54.0f, 54.0f);
        //float yVal = Mathf.Clamp((Random.Range(0.0f + m_randomYMin, m_screenBoundary.y + m_randomYMax)), -54.0f, 54.0f);

        /// Reposition to the next offset.
        //hazard.transform.position = new Vector2(xVal, yVal);
        //SpreadItem(NoOverlap2D, hazard);
        //FindNewPosition(hazard);
    }

    public void SpreadItem(GameObject itemSpreader, GameObject hazard) {
        m_screenBoundMin = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f - (Screen.height / 2), 0.0f));
        Debug.Log("m_screenBoundMin = " + m_screenBoundMin);
        m_screenBoundMax = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 2, Screen.height + (Screen.height / 2), 0.0f));
        Debug.Log("m_screenBoundMax = " + m_screenBoundMax);

        //Vector3 randPosition = new Vector3(Random.Range(-itemXSpread, itemXSpread), Random.Range(-itemYSpread, itemYSpread), Random.Range(-itemZSpread, itemZSpread)) + transform.position;
        ///// The `+ transform.position` makes it an offset from where the ItemAreaSpawner is located.
        m_ItemXSpread = m_screenBoundMax.x;
        m_ItemYSpread = m_screenBoundMax.y;
        Vector2 randPosition = new Vector2(Random.Range(m_screenBoundMin.x, m_screenBoundMax.x), Random.Range(m_screenBoundMin.y, m_screenBoundMax.y));

        //GameObject clone = Instantiate(itemToSpread, randPosition, Quaternion.identity);
        itemToSpread.transform.position = randPosition;
        FindNewPosition(hazard);
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireCube((m_minV2 + m_maxV2) / 2, m_overlapTestBoxScale * 2);

        Gizmos.color = Color.green;
        Gizmos.DrawWireCube((m_screenBoundMin + m_screenBoundMax) / 2, m_screenBoundMax - m_screenBoundMin);
    }

    private void FindNewPosition(GameObject hazard) {
        //RaycastHit hit;

        //if (Physics.Raycast(transform.position, Vector3.down, out hit, raycastDistance)) {

        //    Quaternion spawnRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

        //    Vector3 overlapTestBoxScale = new Vector3(overlapTestBoxSize, overlapTestBoxSize, overlapTestBoxSize);
        //    /// Where line 11 originally was.
        //    //Collider[] collidersInsideOverlapBox = new Collider[1];
        //    int numberOfCollidersFound = Physics.OverlapBoxNonAlloc(hit.point, overlapTestBoxScale, collidersInsideOverlapBox, spawnRotation, spawnedObjectLayer);

        //    Debug.Log("Number Of Colliders Found = " + numberOfCollidersFound);

        //    if (numberOfCollidersFound == 0) {
        //        //Debug.Log("Spawned the game object");
        //        Pick(hit.point, spawnRotation);
        //    } else {
        //        Debug.Log("Name Of Collider 0 Found = " + collidersInsideOverlapBox[0].name);
        //    }
        //}

        m_screenBoundMin = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f - (Screen.height / 2), 0.0f));
        m_screenBoundMax = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 2, Screen.height + (Screen.height / 2), 0.0f));

        //float xVal = Random.Range(m_screenBoundary.x + m_randomOngoingXMin, m_screenBoundary.x + m_randomOngoingXMax);
        float xVal = Random.Range(m_screenBoundMin.x, m_screenBoundMax.x);
        //Debug.Log("m_screenBoundary.x = " + m_screenBoundary.x);
        //Debug.Log("xVal = " + xVal);
        /// The minumum & maximum values came from 10.8 divided by 2.
        /// 10.8 came from the height of the intended play area.
        float yVal = Mathf.Clamp((Random.Range(m_playerCharacter.transform.position.y + m_yOffset + m_randomYMin, m_playerCharacter.transform.position.y + m_yOffset + m_randomYMax)), -52.0f, 52.0f);

        transform.position = new Vector2(xVal, yVal);

        m_overlapTestBoxScale = new Vector3(overlapTestBoxSize * 2.0f, overlapTestBoxSize * 1.5f, 0.0f);
        /// Where line 11 originally was.
        //Collider[] collidersInsideOverlapBox = new Collider[1];
        //int numberOfCollidersFound = Physics.OverlapBoxNonAlloc(hit.point, overlapTestBoxScale, collidersInsideOverlapBox, spawnRotation, spawnedObjectLayer);
        m_minV2 = this.gameObject.transform.position - m_overlapTestBoxScale;
        m_maxV2 = this.gameObject.transform.position + m_overlapTestBoxScale;
        int numberOf2DCollidersFound = Physics2D.OverlapAreaNonAlloc(m_minV2, m_maxV2, m_collidersInsideOverlapBox, spawnedObjectLayer);

        Debug.Log("Number Of 2D Colliders Found = " + numberOf2DCollidersFound);

        if (numberOf2DCollidersFound == 0) {
            //Debug.Log("Spawned the game object");
            Pick(hazard, transform.position, Quaternion.identity);
        } else {
            Debug.Log("Name Of Collider 0 Found = " + m_collidersInsideOverlapBox[0].name);
        }
    }

    private void Pick(GameObject hazard, Vector2 positionToSpawn, Quaternion rotationToSpawn) {
        Debug.Log("Inside Pick(Vector2 positionToSpawn, Quaternion rotationToSpawn)");
        hazard.transform.position = positionToSpawn;
    }

    private float OverlapTestBoxSizeLimit() {
        if (overlapTestBoxSize < m_OverlapTestBoxSizeMinimum) {
            overlapTestBoxSize = m_OverlapTestBoxSizeMinimum;
        }
        return overlapTestBoxSize;
    }

}

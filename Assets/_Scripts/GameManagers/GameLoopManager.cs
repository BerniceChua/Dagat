﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.EventSystems;

public class GameLoopManager : MonoBehaviour {

    [SerializeField] public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases.
    [SerializeField] public float m_EndDelay = 3f;
    [SerializeField] public float m_DelayEndMessage = 3.0f;

    [SerializeField] private GameObject m_youDiedPanel;     // Reference to the overlay Panel that contains Text to display result text, etc.
    //public TextMeshProUGUI m_MessageText;                  // Reference to the overlay Text to display result text, etc.
    public Text m_EndMessageText;                  // Reference to the overlay Text to display result text, etc.
    public Text m_CauseOfDeathText;

    [SerializeField] public PlayerMovement m_playerCharacter;

    private WaitForSeconds m_StartWait;         /// Used to have a delay whilst the round starts.
    private WaitForSeconds m_EndWait;           /// Used to have a delay whilst the round or game ends.
    private WaitForSeconds m_EndWaitCataclysm;  /// Used to delay message and UI after cataclysm appears.

    [SerializeField] public GameObject m_StatsPanel;
    /// Timer-related UI
    [SerializeField] public float m_TimerMaximumInMinutes;
    private float m_TimerMaximumInSeconds;
    [SerializeField] private TextMeshProUGUI m_timerText;
    private float m_currentTime;
    private float m_startTime;
    string minutes;
    string seconds;
    string fraction;

    public float m_RunningTime;

    private float m_timeAtEnding;
    private float newEndingDistance;

    /// <summary>
    /// MainMenu options-related UI (restart, main menu, quit)
    /// </summary>
    [Tooltip("Put MainMenuPanel here.")]
    [SerializeField] private GameObject m_menuPanel;

    /// <summary>
    /// PauseMenu options-related UI (unpause, restart, main menu, quit)
    /// </summary>
    [Tooltip("Put PauseMenuPanel here.")]
    [SerializeField] private GameObject m_pausePanel;

    [Tooltip("For scene loading, put the name of the scene for the intro")]
    [SerializeField] private string m_introScene;
    [Tooltip("For scene loading, put the name of the scene for the game level")]
    [SerializeField] private string m_gameLevelScene;

    public AudioSource EndSoundSource;
    public AudioSource Soundtrack;
    public bool PlayingEndSound;

    [SerializeField] private GameObject m_pausedDefaultButton;

    [SerializeField] private GameObject m_gameOverDefaultButton;

    [SerializeField] private GameObject m_DefaultButton;

    [SerializeField] private GameObject m_CreditsScrollView;

    #region For Scorekeeping
    [SerializeField] private Scorekeeper m_scorekeeper;

    string m_bestTimePelagicZone = "Best Time Pelagic Zone";
    string m_bestTimePelagicZoneText = "Best Time Pelagic Zone Text";

    string m_bestDistancePelagicZone = "Best Distance Pelagic Zone";
    string m_bestDistancePelagicZoneText = "Best Distance Pelagic Zone Text";
    #endregion

    // Use this for initialization
    void Start() {

        /// convert seconds to milliseconds
        m_TimerMaximumInSeconds = m_TimerMaximumInMinutes * 60.0f;

        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);
        m_EndWaitCataclysm = new WaitForSeconds(m_DelayEndMessage);

        //ResetTimer();
        ResetTime();
        StartCoroutine(GameLoop());
    }

    // Update is called once per frame
    void Update() {
        //if (Input.GetButtonDown("Menu")) {
        //    UIButtonPauseGame();
        //    m_pausePanel.SetActive(true);
        //}
    }

    private IEnumerator GameLoop() {
        //ResetTimer();
        // Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
        yield return StartCoroutine(RoundStarting());

        // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
        yield return StartCoroutine(RoundPlaying());

        // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished.
        yield return StartCoroutine(RoundEnding());

        /// Re-enable restart/main menu/quit options when game ends.
        m_menuPanel.SetActive(true);
        /// This makes sure that the UI button is selectable for controllers.
        EventSystem.current.SetSelectedGameObject(m_gameOverDefaultButton);
        //m_gameOverDefaultButton.GetComponent<Button>().Select();
        //private void setButtonsActive(bool set) {
        //    buttons.SetActive(set);
        //    if (set)
        //        EventSystem.current.SetSelectedGameObject(buttons.GetComponentInChildren<Button>().gameObject, null);
        //    else
        //        EventSystem.current.SetSelectedGameObject(null);
        //}
        //m_gameOverDefaultButton.SetActive(true);
        //m_gameOverDefaultButton.GetComponent<Selectable>().transition = ;

        /// Ensures that the button has a select highlight by default.
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.firstSelectedGameObject = m_gameOverDefaultButton.gameObject;
        m_gameOverDefaultButton.GetComponent<Button>().Select();

        PlayingEndSound = false;

        //EndSoundSource.Stop();
        //Soundtrack.Play();
        
        /// After 'RoundEnding()' has finished, check if player wants to play again, or go to main menu, or quit.
        /// These are for either controller buttons or keyboard shortcuts, if the players don't use the UI buttons.
        if (Input.GetKey(KeyCode.Return) ) {
            // Restart the level.
            SceneManager.LoadScene(m_gameLevelScene);
        } else if (Input.GetKey(KeyCode.Escape) ) {
            // Go to main menu.
            SceneManager.LoadScene(m_introScene);
        } else if (Input.GetKey(KeyCode.Q) ) {
            // Quit the game.
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

    }

    private IEnumerator RoundStarting() {
        // As soon as the round starts reset the players and make sure they can't move.
        ResetTimer();
        ResetAllPlayers();
        //DisablePlayerControl();

        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_StartWait;
    }

    private IEnumerator RoundPlaying() {

        /// Disable restart/main menu/quit options when game starts.
        m_menuPanel.SetActive(false);

        /// As soon as the round begins playing let the players control the characters.
        EnablePlayerControl();

        /// Clear the text from the screen.
        //m_CauseOfDeathText.text = string.Empty; 
        m_EndMessageText.text = string.Empty;

        /// As soon as the round begins playing...
        while (m_playerCharacter.HasCollided == false) {
            /// Put this here inside the while loop so that 
            /// players can only see the pause menu while 
            /// the game is running, and cannot 
            /// open the pause menu when it's game over.
            if (Input.GetButtonDown("Menu")) {
                UIButtonPauseGame();
                m_pausePanel.SetActive(true);

                /// Ensures that the button has a select highlight by default.
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.firstSelectedGameObject = m_pausedDefaultButton.gameObject;
                m_pausedDefaultButton.GetComponent<Button>().Select();
            }

            /// ... start the timer.
            UpdateTimer();
            // ... return on the next frame.
            yield return null;
        }
        // Stop players from moving when it's game over.  
        DisablePlayerControl();

        m_timeAtEnding = m_currentTime;
        PlayerPrefs.SetFloat(m_bestTimePelagicZone, m_timeAtEnding);
        PlayerPrefs.SetString(m_bestTimePelagicZoneText, m_timerText.text);
        m_scorekeeper.FindHigherTime(m_bestTimePelagicZone, m_currentTime);
        m_scorekeeper.FindHigherDistance(m_bestDistancePelagicZone, newEndingDistance);
        m_scorekeeper.FindHigherDistance(m_bestDistancePelagicZoneText, newEndingDistance);


        m_StatsPanel.SetActive(false);

        //yield return m_EndWaitCataclysm;
    }

    private IEnumerator RoundEnding() {
        // Stop players from moving.
        DisablePlayerControl();

        /// Clear the result from the previous round.

        //// See if there is a casualty now the round is over.
        //m_RoundCasualty = FindCasualty();
        if (PlayerPrefs.GetFloat(m_bestTimePelagicZone) <= m_currentTime) {
            //m_gameOverDisplay.GetComponentInChildren<Text>().color = Color.green;
            //Debug.Log("Special message should appear... in 3... 2... 1:");
            yield return new WaitForSecondsRealtime(m_EndDelay);
            //m_fireworks.Emit(100);
            //m_newHighScore.SetActive(true);
            //m_textAnimator.SetBool("AppearOnScreen", true);
        }

        yield return m_DelayEndMessage;

        /// Get a message based on the scores and whether or not all the characters survived and display it.
        string causeOfDeathMessage = CauseOfDeathMessage();
        m_CauseOfDeathText.text += causeOfDeathMessage;
        string message = EndMessage();
        m_EndMessageText.text += message;

        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_EndWait;
    }

    private string EndMessage() {
        // By default when a round ends, and all the characters survived, show the victory message.
        string message = "";

        // Let players know how far they travelled & how much time passed before game over.
        if (m_playerCharacter.HasCollided) {
            m_youDiedPanel.SetActive(true);
            message = "Distance = " + m_playerCharacter.GetDistanceString() + " | " + " Time = " + DisplayFormattedTime(m_timeAtEnding);
        }

        return message;
    }

    private string CauseOfDeathMessage() {
        return m_playerCharacter.CollidedInfo;
    }

    // This function is used to turn all the player-characters back on and reset their positions and properties.
    private void ResetAllPlayers() {
        //for (int i = 0; i < m_PlayersArray.Length; i++) {
        //    m_PlayersArray[i].Reset();
        //}
    }

    private void EnablePlayerControl() {
        m_playerCharacter.EnableControl();
    }

    private void DisablePlayerControl() {
        m_playerCharacter.DisableControl();
    }

#region Timer-related Logic
    /// <summary>
    /// This function is used to reset the timer.
    /// </summary>
    private void ResetTimer() {
        //m_currentTime = m_TimerMaximumInSeconds;
        //minutes = Mathf.Floor(m_currentTime / 60).ToString("00");
        //seconds = (m_currentTime % 60).ToString("00");
        //fraction = ((m_currentTime * 100) % 100).ToString("000");
        //m_countdownTimerText.text = "Time Left: " + minutes + ":" + seconds + ":" + fraction;

        m_currentTime = 0.0f;
    }

    private void UpdateTimer() {
        //if (m_countdownTimerText != null) {
        //    m_currentTime -= Time.deltaTime;
        //    minutes = Mathf.Floor(m_currentTime / 60).ToString("00");
        //    seconds = (m_currentTime % 60).ToString("00");
        //    fraction = ((m_currentTime * 100) % 100).ToString("000");
        //    m_countdownTimerText.text = "Time Left: " + minutes + ":" + seconds + ":" + fraction;
        //}

        //if (m_currentTime < 7 && !PlayingEndSound) {
        //    Soundtrack.Stop();
        //    EndSoundSource.Play();
        //    PlayingEndSound = true;
        //}

        //if (m_timerText != null) {
            m_currentTime += Time.deltaTime;
            //m_RunningTime = Time.time - m_startTime;
            //m_currentTime += Time.deltaTime - m_startTime;
            //m_timerText.text = minutes + ":" + seconds + ":" + fraction;
            //m_timerText.text = DisplayFormattedTime(m_RunningTime);
            m_timerText.text = DisplayFormattedTime(m_currentTime);
        //}
    }

    public string DisplayFormattedTime(float unformattedTime) {
        //minutes = Mathf.Floor(m_currentTime / 60).ToString("00");
        //seconds = (m_currentTime % 60).ToString("00");
        //fraction = ((m_currentTime * 100) % 100).ToString("000");

        string hours = ((int)unformattedTime / 3600).ToString("00");
        string minutes = ((int)unformattedTime / 60).ToString("00");
        //string seconds = ((int) t % 60).ToString("00");
        //string seconds = string.Format("{0:00.00}", (t % 60).ToString("00"));
        //string seconds = (unformattedTime % 60).AddOneLeadingZero();
        string seconds = ((int)unformattedTime % 60).ToString("00");

        return hours + ":" + minutes + ":" + seconds;
    }

    public void ResetTime() {
        m_startTime = Time.time;
        UpdateTimer();
    }
    #endregion




#region UI Controls
    public void PressEscToClose() {
        if (m_CreditsScrollView.activeInHierarchy && Input.GetButtonDown("Menu")) {
            m_CreditsScrollView.SetActive(false);

            /// This makes sure that the UI button is selectable for controllers.
            /// Ensures that the button has a select highlight by default.
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.firstSelectedGameObject = m_DefaultButton.gameObject;
            m_DefaultButton.GetComponent<Button>().Select();
        }
    }

    /// <summary>
    /// UI Button to restart the level.
    /// </summary>
    public void UIButtonRestartGame() {
        SceneManager.LoadScene(m_gameLevelScene);
    }

    /// <summary>
    /// Pause the game by setting the timescale to 0.  
    /// </summary>
    public void UIButtonPauseGame() {
        //Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        Time.timeScale = 0;

        /// This makes sure that the UI button is selectable for controllers.
        EventSystem.current.SetSelectedGameObject(m_pausedDefaultButton);
    }

    /// <summary>
    /// Unpause the game by setting the timescale back to 1.  
    /// </summary>
    public void UIButtonUnpauseGame() {
        //Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        Time.timeScale = 1;
    }

    /// <summary>
    /// UI Button to go to the main menu.
    /// </summary>
    public void UIButtonGoToMainMenu() {
        SceneManager.LoadScene(m_introScene);
    }

    /// <summary>
    /// UI Button to quit the game.
    /// </summary>
    public void UIButtonQuitGame() {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }
#endregion
}

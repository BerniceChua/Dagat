﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorekeeper : MonoBehaviour {
    bool m_isNewTimeHigher = false;
    bool m_isNewDistanceHigher = false;

    //// Use this for initialization
    //void Start () {

    //}

    //// Update is called once per frame
    //void Update () {

    //}

    public float ShowCurrentHighestTime(string scoreName) {
        return PlayerPrefs.GetFloat(scoreName);
    }

    public float ShowCurrentHighestDistance(string scoreName) {
        return PlayerPrefs.GetFloat(scoreName);
    }

    //public void FindHigherScore(string scoreName, float oldScore, float newScore) {
    public void FindHigherTime(string scoreName, float newEndingTime) {
        //if (PlayerPrefs.GetFloat("Best Time") < newScore)
        Debug.Log(IsNewTimeHigher(newEndingTime));
        if (IsNewTimeHigher(newEndingTime)) {
            //if (IsNewScoreHigher()) {
            PlayerPrefs.SetFloat(scoreName, newEndingTime);
            m_isNewTimeHigher = true;
        } else {
            m_isNewTimeHigher = false;
        }

        //PlayerPrefs.SetFloat(scoreName, newScore);
        return;
    }

    private bool IsNewTimeHigher(float newScore) {
        //public bool IsNewScoreHigher() {
        //return m_isNewScoreHigher;
        //Debug.Log("PlayerPrefs.GetFloat('Best Time') < newScore) = " + (PlayerPrefs.GetFloat("Best Time") < newScore) );
        Debug.Log(PlayerPrefs.GetFloat("Best Time"));
        Debug.Log(newScore);
        return (PlayerPrefs.GetFloat("Best Time") < newScore);
    }

    public void FindHigherDistance(string scoreName, float newEndingDistance) {
        //if (PlayerPrefs.GetFloat("Best Time") < newScore)
        Debug.Log(IsNewDistanceHigher(newEndingDistance));
        if (IsNewDistanceHigher(newEndingDistance)) {
            //if (IsNewScoreHigher()) {
            PlayerPrefs.SetFloat(scoreName, newEndingDistance);
            m_isNewDistanceHigher = true;
        } else {
            m_isNewDistanceHigher = false;
        }

        //PlayerPrefs.SetFloat(scoreName, newScore);
        return;
    }

    private bool IsNewDistanceHigher(float newScore) {
        //public bool IsNewScoreHigher() {
        //return m_isNewScoreHigher;
        //Debug.Log("PlayerPrefs.GetFloat('Best Time') < newScore) = " + (PlayerPrefs.GetFloat("Best Time") < newScore) );
        Debug.Log(PlayerPrefs.GetFloat("Best Time"));
        Debug.Log(newScore);
        return (PlayerPrefs.GetFloat("Best Time") < newScore);
    }

}

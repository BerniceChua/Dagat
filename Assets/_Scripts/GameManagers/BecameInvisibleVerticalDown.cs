﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BecameInvisibleVerticalDown : MonoBehaviour
{
    private GameObject m_playerCharacter;

    private HazardsManagerVerticalDown m_hazardsManagerDown;

    private Vector2 m_screenBoundary;
    private Vector2 m_screenBottomRight;
    private Vector2 m_screenBottomLeft;

    private void OnEnable() {
        m_playerCharacter = GameObject.FindGameObjectWithTag("Player");

        m_hazardsManagerDown = GameObject.FindObjectOfType<HazardsManagerVerticalDown>();
        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z) );
        m_screenBottomRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, Camera.main.transform.position.z));
        m_screenBottomLeft = Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z));
    }

    private void LateUpdate() {
        //if ((this.gameObject.transform.position.y > m_screenBoundary.y*1.5f) || (this.gameObject.transform.position.y < -m_screenBoundary.y*1.5f)) {
        //    /// Recycle the hazards
        //    m_hazardsManager.RecycleHazards(this.gameObject);
        //}

        /// Checking the player's position (clamped between 
        /// -46.0f & 46.0f will ensure that these don't 
        /// spawn when the player is near the bottom or the top.  
        /// It will give players room to maneuver.  Also, this 
        /// stops the hazards from popping in randomly.  

        if ((m_playerCharacter.transform.position.y > -46.0f) && (m_playerCharacter.transform.position.y < 46.0f) && (this.gameObject.GetComponent<SpriteRenderer>().isVisible == false) && (this.gameObject.transform.position.x < m_hazardsManagerDown.m_playerCharacter.transform.position.x)) {
            /// Recycle the hazards
            m_hazardsManagerDown.RecycleHazards(this.gameObject);
        }

        if ((m_playerCharacter.transform.position.y > -46.0f) && (m_playerCharacter.transform.position.y < 46.0f) && (this.gameObject.GetComponent<SpriteRenderer>().isVisible == false) && (this.gameObject.transform.position.y > m_screenBoundary.y)) {
            /// Recycle the hazards
            m_hazardsManagerDown.RecycleHazards(this.gameObject);
        }

        //if ((this.gameObject.GetComponent<SpriteRenderer>().isVisible == false) && (this.gameObject.transform.position.y > m_screenBottomLeft.y)) {
        //    /// Recycle the hazards
        //    m_hazardsManagerDown.RecycleHazards(this.gameObject);
        //}
    }

}

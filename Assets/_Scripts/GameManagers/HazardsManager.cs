﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardsManager : MonoBehaviour
{
    [SerializeField] private float[] lengthAndWidths;

    [SerializeField] private GameObject[] m_hazardsPrefabs;

    [SerializeField] private float m_randomStartingXMin = -2;
    [SerializeField] private float m_randomStartingXMax = 5;
    [SerializeField] private float m_randomYMin = -6;
    [SerializeField] private float m_randomYMax = 6;

    [SerializeField] private float m_randomOngoingXMin = -5;
    [SerializeField] private float m_randomOngoingXMax = 5;

    [SerializeField] public GameObject m_playerCharacter;
    private float m_xOffset;
    private float m_yOffset;

    private Vector2 m_screenBoundary;

    // Start is called before the first frame update
    void Start()
    {
        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

        for (int i = 0; i < m_hazardsPrefabs.Length; i++) {
            // Get a prefab (shark, jellyfish, plastic bag)
            // Find out distance from each other
                // For each PREFAB add a width and height floating point array (SAME SIZE - length as your hazards)
                // First we need to find the length and width of object
            // Allow the code to make their distances closer

            Extents2D thisExtent = new Extents2D();

            float xVal = Random.Range(m_screenBoundary.x + m_randomStartingXMin, m_screenBoundary.x + m_randomStartingXMax);
            float yVal = Random.Range(m_randomYMin, m_randomYMax);
            //Debug.Log("instantiating " + m_hazardsPrefabs[i].name + xVal);

            var thisGameObj = Instantiate(m_hazardsPrefabs[i], new Vector2(xVal, yVal), Quaternion.Euler(0, 0, 0));

            //m_xOffset = 20.0f;
            //m_yOffset = 0.0f;
            m_xOffset = xVal;
            m_yOffset = yVal;

            //m_hazardsPrefabs[i].GetComponent<MarineLifeMovement>().m_PosOffset = new Vector2(xVal, yVal);

            thisGameObj.AddComponent<BecameInvisible>();

            // For a cleaner hierachy, parent this game object to this hazards manager.
            thisGameObj.transform.SetParent(this.transform);

        }
    }

    // Update is called once per frame
    void Update() {
        //m_xOffset = m_playerCharacter.transform.position.x;
        //m_yOffset = m_playerCharacter.transform.position.y;
    }

    public void RecycleHazards(GameObject hazard) {
        //Debug.Log("Recycling " + hazard.name);

        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        //float xVal = Random.Range(m_playerCharacter.transform.position.x + m_xOffset + m_randomXMin, m_playerCharacter.transform.position.x + m_xOffset + m_randomXMax);
        //float xVal = Random.Range(m_screenBoundary.x + m_randomXMin, m_screenBoundary.x + m_randomXMax);
        //float xVal = Random.Range(m_randomXMin, m_randomXMax);
        float xVal = Random.Range(m_screenBoundary.x + m_randomOngoingXMin, m_screenBoundary.x + m_randomOngoingXMax);
        //float xVal = Random.Range(m_playerCharacter.transform.position.x + m_randomOngoingXMin, m_playerCharacter.transform.position.x + m_randomOngoingXMax);

        //Debug.Log("m_screenBoundary.x = " + m_screenBoundary.x);
        //Debug.Log("xVal = " + xVal);
        /// The minumum & maximum values came from 10.8 divided by 2.
        /// 10.8 came from the height of the intended play area.
        //float yVal = Mathf.Clamp((Random.Range(m_playerCharacter.transform.position.y + m_yOffset + m_randomYMin, m_playerCharacter.transform.position.y + m_yOffset + m_randomYMax)), -54.0f, 54.0f);
        float yVal = Mathf.Clamp((Random.Range(m_playerCharacter.transform.position.y + m_yOffset + m_randomYMin, m_playerCharacter.transform.position.y + m_yOffset + m_randomYMax)), -52.0f, 52.0f);
        //float yVal = Mathf.Clamp((Random.Range(-m_screenBoundary.y + m_randomYMin, m_screenBoundary.y + m_randomYMax)), -54.0f, 54.0f);
        //float yVal = Mathf.Clamp((Random.Range(0.0f + m_randomYMin, m_screenBoundary.y + m_randomYMax)), -54.0f, 54.0f);

        /// Reposition to the next offset.
        hazard.transform.position = new Vector2(xVal, yVal);
        //hazard.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(Screen.width + xVal, Random.Range(Screen.height + m_randomXMin, Screen.height + m_randomXMax), 0.0f));

        //m_xOffset = m_playerCharacter.transform.position.x;
        //m_yOffset = m_playerCharacter.transform.position.y;
    }
}

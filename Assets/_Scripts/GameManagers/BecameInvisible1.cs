﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BecameInvisible1 : MonoBehaviour
{
    private GameObject m_playerCharacter;

    private HazardsManager2 m_hazardsManager;

    private Vector2 m_screenBoundary;
    private Vector2 m_screenBottomRight;
    private Vector2 m_screenBottomLeft;

    private void OnEnable() {
        m_playerCharacter = GameObject.FindGameObjectWithTag("Player");

        m_hazardsManager = GameObject.FindObjectOfType<HazardsManager2>();
        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z) );
        m_screenBottomRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, Camera.main.transform.position.z));
        m_screenBottomLeft = Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z));
    }

    private void LateUpdate() {
        //if ((this.gameObject.transform.position.y > m_screenBoundary.y*1.5f) || (this.gameObject.transform.position.y < -m_screenBoundary.y*1.5f)) {
        //    /// Recycle the hazards
        //    m_hazardsManager.RecycleHazards(this.gameObject);
        //}

        //if ((m_playerCharacter.transform.position.y > -53.0f) && (m_playerCharacter.transform.position.y < 53.0f)) {
        //if ((this.gameObject.GetComponent<SpriteRenderer>().isVisible == false) && (this.gameObject.transform.position.x < m_hazardsManager.m_playerCharacter.transform.position.x)) {
        //if (this.gameObject.GetComponent<SpriteRenderer>().isVisible == false)
        //{
        //    if (this.gameObject.transform.position.x < m_hazardsManager.m_playerCharacter.transform.position.x)
        //    {
        //        /// Recycle the hazards
        //        m_hazardsManager.RecycleHazards(this.gameObject);
        //    }
        //}
        bool isInvisible = this.gameObject.GetComponent<SpriteRenderer>().isVisible == false;
        bool isBehindPlayerAvatar = this.gameObject.transform.position.x < m_hazardsManager.m_playerCharacter.transform.position.x;
        if (isInvisible && isBehindPlayerAvatar) {
            /// Recycle the hazards
            m_hazardsManager.RecycleHazards(this.gameObject);
        }
        //if (isBehindPlayerAvatar) {
        //    /// Recycle the ItemSpreader2D
        //    //m_hazardsManager.SpreadItem();
        //    m_hazardsManager.SpreadItem(this.gameObject);

        //    if (isInvisible) { 
        //        /// Recycle the hazards
        //        m_hazardsManager.RecycleHazards(this.gameObject);
        //    }
        //}
        //}
        //if ((this.gameObject.GetComponent<SpriteRenderer>().isVisible == false) && (this.gameObject.transform.position.y > m_screenBoundary.y)) {
        //    /// Recycle the hazards
        //    m_hazardsManager.RecycleHazards(this.gameObject);
        //}

        //if ((this.gameObject.GetComponent<SpriteRenderer>().isVisible == false) && (this.gameObject.transform.position.y < m_screenBottomLeft.y)) {
        //    /// Recycle the hazards
        //    m_hazardsManager.RecycleHazards(this.gameObject);
        //}
    }

    //private void OnBecameInvisible() {
    //    /// Recycle the hazards
    //    m_hazardsManager.RecycleHazards(this.gameObject);

    //    //if (this.gameObject.transform.position.x < (m_hazardsManager.m_playerCharacter.transform.position.x) ) {
    //    //    /// Recycle the hazards
    //    //    m_hazardsManager.RecycleHazards(this.gameObject);
    //    //}

    //    //if (this.gameObject.GetComponent<SpriteRenderer>().isVisible == false && this.gameObject.transform.position.x < (m_hazardsManager.m_playerCharacter.transform.position.x)) {
    //    //    /// Recycle the hazards
    //    //    m_hazardsManager.RecycleHazards(this.gameObject);
    //    //}

    //}
}

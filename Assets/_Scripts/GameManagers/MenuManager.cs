﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    [Tooltip("For scene loading, put the name of the scene for the intro")]
    [SerializeField] private string m_introScene;
    [Tooltip("For scene loading, put the name of the scene for the game level")]
    [SerializeField] private string m_gameLevelScene;

    [SerializeField] public GameObject m_DefaultButton;

    [SerializeField] private GameObject m_CreditsScrollView;

    public void UISetAsDefaultButton(GameObject defaultButton) {
        m_DefaultButton = defaultButton;
    }

    public void PressEscToClose() {
        if (m_CreditsScrollView.activeInHierarchy && Input.GetButtonDown("Menu")) {
            m_CreditsScrollView.SetActive(false);

            /// This makes sure that the UI button is selectable for controllers.
            /// Ensures that the button has a select highlight by default.
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.firstSelectedGameObject = m_DefaultButton.gameObject;
            m_DefaultButton.GetComponent<Button>().Select();
        }
    }

    /// <summary>
    /// UI Button to restart the level.
    /// </summary>
    public void UIButtonRestartGame() {
        SceneManager.LoadScene(m_gameLevelScene);
    }

    /// <summary>
    /// Pause the game by setting the timescale to 0.  
    /// </summary>
    public void UIButtonPauseGame() {
        //Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        Time.timeScale = 0;

        EventSystem.current.SetSelectedGameObject(m_DefaultButton);
        /// This makes sure that the UI button is selectable for controllers.
        /// Ensures that the button has a select highlight by default.
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.firstSelectedGameObject = m_DefaultButton.gameObject;
        m_DefaultButton.GetComponent<Button>().Select();
    }

    /// <summary>
    /// Unpause the game by setting the timescale back to 1.  
    /// </summary>
    public void UIButtonUnpauseGame() {
        //Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        Time.timeScale = 1;
    }

    /// <summary>
    /// UI Button to go to the main menu.
    /// </summary>
    public void UIButtonGoToMainMenu() {
        SceneManager.LoadScene(m_introScene);
    }

    /// <summary>
    /// UI Button to quit the game.
    /// </summary>
    public void UIButtonQuitGame() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}

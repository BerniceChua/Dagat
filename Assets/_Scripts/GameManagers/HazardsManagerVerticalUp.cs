﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardsManagerVerticalUp : MonoBehaviour
{
    [SerializeField] private GameObject[] m_hazardsPrefabs;

    [SerializeField] private float m_randomStartingXMin = -45;
    [SerializeField] private float m_randomStartingXMax = 1;
    [SerializeField] private float m_randomStartingYMin = 1;
    [SerializeField] private float m_randomStartingYMax = 10;

    [SerializeField] private float m_randomYMin = 5;
    [SerializeField] private float m_randomYMax = 15;

    [SerializeField] private float m_randomOngoingXMin = -10;
    [SerializeField] private float m_randomOngoingXMax = 1;

    [SerializeField] public GameObject m_playerCharacter;
    private float m_xOffset;
    private float m_yOffset;

    private Vector2 m_screenBoundary;

    // Start is called before the first frame update
    void Start()
    {
        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

        for (int i = 0; i < m_hazardsPrefabs.Length; i++) {
            float xVal = Random.Range(m_screenBoundary.x + m_randomStartingXMin, m_screenBoundary.x + m_randomStartingXMax);
            float yVal = Random.Range(m_randomYMin, m_randomYMax);
            //Debug.Log("instantiating vertical up " + m_hazardsPrefabs[i].name + " (" + xVal + ", " + yVal + ")");

            var thisGameObj = Instantiate(m_hazardsPrefabs[i], new Vector2(xVal, yVal), Quaternion.Euler(0, 0, 0));

            thisGameObj.AddComponent<BecameInvisibleVerticalUp>();

            // For a cleaner hierachy, parent this game object to this hazards manager.
            thisGameObj.transform.SetParent(this.transform);

        }
    }

    // Update is called once per frame
    void Update() {
        //m_xOffset = m_playerCharacter.transform.position.x;
        //m_yOffset = m_playerCharacter.transform.position.y;
    }

    public void RecycleHazards(GameObject hazard) {
        //Debug.Log("Recycling " + hazard.name);

        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        //float xVal = Random.Range(m_playerCharacter.transform.position.x + m_xOffset + m_randomXMin, m_playerCharacter.transform.position.x + m_xOffset + m_randomXMax);
        //float xVal = Random.Range(m_screenBoundary.x + m_randomXMin, m_screenBoundary.x + m_randomXMax);
        //float xVal = Random.Range(m_randomXMin, m_randomXMax);
        float xVal = Random.Range(m_screenBoundary.x + m_randomOngoingXMin, m_screenBoundary.x + m_randomOngoingXMax);
        //float xVal = Random.Range(m_playerCharacter.transform.position.x + m_randomOngoingXMin, m_playerCharacter.transform.position.x + m_randomOngoingXMax);

        //Debug.Log("m_screenBoundary.x = " + m_screenBoundary.x);
        //Debug.Log("xVal = " + xVal);
        /// The minumum & maximum values came from 10.8 divided by 2.
        /// 10.8 came from the height of the intended play area.
        //float yVal = Mathf.Clamp((Random.Range(m_playerCharacter.transform.position.y + m_yOffset + m_randomYMin, m_playerCharacter.transform.position.y + m_yOffset + m_randomYMax)), -54.0f, 54.0f);
        //float yVal = Mathf.Clamp((Random.Range(-m_screenBoundary.y + m_randomYMin, m_screenBoundary.y + m_randomYMax)), -54.0f, 54.0f);
        //float yVal = Mathf.Clamp((Random.Range(0.0f + m_randomYMin, m_screenBoundary.y + m_randomYMax)), -54.0f, 54.0f);
        //float yVal = Mathf.Clamp((Random.Range(m_screenBoundary.y + m_randomYMin, m_screenBoundary.y + m_randomYMax)), -45.0f, 45.0f);
        float yVal = (Random.Range(m_screenBoundary.y + m_randomYMin, m_screenBoundary.y + m_randomYMax));

        /// Reposition to the next offset.
        hazard.transform.position = new Vector2(xVal, yVal);
        //hazard.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(Screen.width + xVal, Random.Range(Screen.height + m_randomXMin, Screen.height + m_randomXMax), 0.0f));

        //m_xOffset = m_playerCharacter.transform.position.x;
        //m_yOffset = m_playerCharacter.transform.position.y;
    }
}

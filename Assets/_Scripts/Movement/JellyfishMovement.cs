﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JellyfishMovement : HazardsMovement {

    //private Animator m_animator;

    private bool m_floatUp;

    float originalYPosition;

    public float amplitude = 0.5f;
    public float frequency = 1f;
    // Position Storage Variables
    //Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();

    private HazardsManager m_hazardsManager;

    [SerializeField] protected float m_rotationSpeed = 5.0f;

    [SerializeField] private enum RotationAxis {
        XAxis,
        YAxis,
        ZAxis,
    };

    [SerializeField] private RotationAxis m_rotationAxis;

    // Start is called before the first frame update
    void Start() {
        base.Start();

        //m_animator = GetComponent<Animator>();
        //m_animator.StartPlayback();

        m_floatUp = false;

        //this.originalY = this.transform.position.y;

        //GetSpriteBounds();
    }

    private void OnBecameVisible() {
        //this.originalYPosition = this.transform.position.y;
        //Debug.Log("transform.position of " + this.gameObject.name + " = " + transform.localPosition);
        m_PosOffset = transform.position;
        //Debug.Log("jellyfish pos offset" + m_PosOffset);
        //Debug.Log("transform.position = posOffset of " + this.gameObject.name + " = " + posOffset);
    }

    //// Update is called once per frame
    void Update() {
        //Debug.Log("jellyfish update");
        float upAndDownSwimTranslate = m_UpAndDownSpeed * Time.deltaTime;
        float forwardSwimTranslate = m_ForwardSwimSpeed * Time.deltaTime;

        //if (m_floatUp) {
        //    FloatingUp(upAndDownSwimTranslate);
        //} else if (!m_floatUp) {
        //    FloatingDown(upAndDownSwimTranslate);
        //}

        //transform.position = new Vector3(transform.position.x,
        //    originalY + ((float)Mathf.Sin(Time.deltaTime) * upAndDownSwimTranslate),
        //    transform.position.z);

        /// Float up/down with a Sin()
        //Debug.Log("posOffset = " + posOffset);
        //tempPos = m_PosOffset;
        //tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        //transform.position = tempPos;

        //transform.Rotate(5.0f, 0.0f, 0.0f, Space.Self);
        //transform.Rotate(m_rotationSpeed, 0.0f, 0.0f, Space.Self);
        //transform.RotateAround(Vector3.left, Vector3.zero, 20 * Time.deltaTime);
        transform.Rotate(AxisOfRotation(), Space.Self);
    }

    void FixedUpdate() {
        //m_playerRigidBody.AddForce(Vector2.right * m_ForwardSwimSpeed);

        //if (Input.GetButtonDown("Vertical")) {
        //    m_playerRigidBody.transform.Translate(Vector2.up * m_UpAndDownSpeed * Time.fixedDeltaTime);
        //}
    }

    //private IEnumerator FloatingUp(float upAndDownSwimTranslate) {
    //    //this.transform.Translate(0, upAndDownSwimTranslate, 0);

    //    /// Move translation along the object's x-axis
    //    CharacterRigidBody.transform.Translate(0, upAndDownSwimTranslate, 0);

    //    yield return new WaitForSeconds(1);
    //    m_floatUp = false;
    //}

    //private IEnumerator FloatingDown(float upAndDownSwimTranslate) {
    //    //this.transform.Translate(0, -upAndDownSwimTranslate, 0);

    //    /// Move translation along the object's x-axis
    //    CharacterRigidBody.transform.Translate(0, -upAndDownSwimTranslate, 0);

    //    yield return new WaitForSeconds(1);
    //    m_floatUp = true;
    //}

    public Vector3 AxisOfRotation() {
        switch (m_rotationAxis) {
            case RotationAxis.XAxis:
                return new Vector3(m_rotationSpeed, 0.0f, 0.0f);
            case RotationAxis.YAxis:
                return new Vector3(0.0f, m_rotationSpeed, 0.0f);
            case RotationAxis.ZAxis:
                return new Vector3(0.0f, 0.0f, m_rotationSpeed);
            default:
                return new Vector3(0.0f, 0.0f, 0.0f);
        }
    }

}

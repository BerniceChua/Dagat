﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkMovement : HazardsMovement {

    //private Rigidbody2D m_playerRigidBody;

    // Start is called before the first frame update
    void Start() {
        base.Start();
        //m_playerRigidBody = this.transform.GetComponent<Rigidbody2D>();
    }

    //// Update is called once per frame
    void Update() {
        float forwardSwimTranslate;

        forwardSwimTranslate = m_ForwardSwimSpeed * Time.deltaTime;

        // Move translation along the object's x-axis
        CharacterRigidBody.transform.Translate(forwardSwimTranslate, 0, 0);
        //this.transform.Translate(forwardSwimTranslate, 0, 0);
    }

    void FixedUpdate() {

    }

}

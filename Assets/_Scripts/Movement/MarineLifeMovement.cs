﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarineLifeMovement : MonoBehaviour {

    [SerializeField] protected GameLoopManager m_GameLoopManager;

    public float m_ForwardSwimSpeed = 5.0f;
    public float m_UpAndDownSpeed = 3.5f;

    protected Rigidbody2D m_characterRigidBody;
    protected Rigidbody2D CharacterRigidBody {
        get {
            //Some other code
            return m_characterRigidBody = this.transform.GetComponent<Rigidbody2D>();
        }
        set {
            //Some other code
            m_characterRigidBody = value;
        }
    }

    public Vector2 m_PosOffset;

    /// <summary>
    /// from https://forum.unity.com/threads/check-the-x-and-y-length-of-a-sprite.229949/
    /// </summary>
    //public SpriteRenderer m_Sprite;
    public float m_RightExtent;
    public float m_LeftExtent;
    public float m_UpExtent;
    public float m_DownExtent;
    protected SpriteRenderer m_Sprite;
    public SpriteRenderer TheSprite {
        get {
            //Some other code
            return m_Sprite = this.transform.GetComponent<SpriteRenderer>();
        }
        set {
            //Some other code
            m_Sprite = value;
        }
    }

    //// Start is called before the first frame update
    //protected virtual void Start() {
    //    //m_characterRigidBody = this.transform.GetComponent<Rigidbody2D>();
    //    //m_Sprite = this.transform.GetComponent<SpriteRenderer>();
    //    //GetSpriteBounds();
    //}

    // Update is called once per frame
    void Update() {

        float upAndDownSwimTranslate = Input.GetAxis("Vertical") * m_UpAndDownSpeed;
        float forwardSwimTranslate;

        // Make it move 10 meters per second instead of 10 meters per frame...
        upAndDownSwimTranslate *= Time.deltaTime;
        forwardSwimTranslate = m_ForwardSwimSpeed * Time.deltaTime;

        // Move translation along the object's x-axis
        m_characterRigidBody.transform.Translate(forwardSwimTranslate, 0, 0);

        if (Input.GetButton("Vertical")) {
            m_characterRigidBody.transform.Translate(Vector2.up * upAndDownSwimTranslate);
        }
    }

    void FixedUpdate() {
        //m_playerRigidBody.AddForce(Vector2.right * m_ForwardSwimSpeed);

        //if (Input.GetButtonDown("Vertical")) {
        //    m_playerRigidBody.transform.Translate(Vector2.up * m_UpAndDownSpeed * Time.fixedDeltaTime);
        //}
    }
}

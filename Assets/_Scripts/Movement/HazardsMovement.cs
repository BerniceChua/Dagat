﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardsMovement : MarineLifeMovement {

    protected virtual void Start() {
        //m_characterRigidBody = this.transform.GetComponent<Rigidbody2D>();
        //m_Sprite = this.transform.GetComponent<SpriteRenderer>();
        GetSpriteBounds();
    }

    public void GetSpriteBounds() {
        m_Sprite = GetComponent<SpriteRenderer>(); //Set the reference to our SpriteRenderer component
        //m_sprite.bounds.extents.x == Distance to the right side, from your center point
        //-m_sprite.bounds.extents.x == Distance to the left side
        //m_sprite.bounds.extents.y == Distance to the top
        //-m_sprite.bounds.extents.y == Distance to the bottom
        Debug.Log("~~~~~~~~~~~~~~~~~m_sprite.bounds.extents = " + m_Sprite.bounds.extents + " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        m_RightExtent = m_Sprite.bounds.extents.x;
        m_LeftExtent = -m_Sprite.bounds.extents.x;
        m_UpExtent = m_Sprite.bounds.extents.y;
        m_DownExtent = -m_Sprite.bounds.extents.y;
    }

}

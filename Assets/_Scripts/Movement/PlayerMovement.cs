﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MarineLifeMovement {

    [SerializeField] private GameLoopManager m_gameLoopManager;

    [SerializeField] private GameObject m_deepSeaPressureAlert;

    [SerializeField] private GameObject m_seagullPrefab;

    private string m_collidedInfo;
    public string CollidedInfo {
        get { return m_collidedInfo; }
    }
    private bool m_hasCollided = false;
    public bool HasCollided {
        get { return m_hasCollided; }
    }

    /// Start is called before the first frame update
    //void Start() {

    //}

    /// Update is called once per frame
    void Update() {
        /// Logic that turns on glowing red that warns about deep-sea pressure death.
        if (transform.position.y <= -54.0f) {
            m_deepSeaPressureAlert.GetComponent<Animator>().enabled = true;

            /// Logic that starts deep-sea pressure death.
            if (transform.position.y <= -59.0f) {
                DeepSeaPressureDeath();
            }
        }

        /// Logic that turns off glowing red that warns about deep-sea pressure death.
        if (transform.position.y > -54.0f) {
            m_deepSeaPressureAlert.GetComponent<Image>().enabled = false;
            //m_deepSeaPressureAlert.GetComponent<Animator>().StopPlayback();
            m_deepSeaPressureAlert.GetComponent<Animator>().enabled = false;
        }

        /// Logic that starts seagull-caused death.
        if (transform.position.y > 54.0f) {
            SeagullDeath();
        }

        #region Keyboard Movement Control
        /// Movement using WASD or arrows.  
        float upAndDownSwimTranslate = Input.GetAxis("Vertical") * m_UpAndDownSpeed;
        float forwardSwimTranslate;

        upAndDownSwimTranslate *= Time.deltaTime;
        forwardSwimTranslate = m_ForwardSwimSpeed * Time.deltaTime;

        /// Move translation along the object's x-axis
        CharacterRigidBody.transform.Translate(forwardSwimTranslate, 0, 0);

        if (Input.GetButton("Vertical") == true) {
            CharacterRigidBody.transform.Translate(Vector2.up * upAndDownSwimTranslate);
        }
#endregion

        #region Controller Left Joystick Movement Control
        /// Movement using left analog joystick.  
        float translation = Input.GetAxis("Vertical") * m_UpAndDownSpeed;
        translation *= Time.deltaTime;
        CharacterRigidBody.transform.Translate(Vector2.up * translation);
        #endregion
    }

    void FixedUpdate() {

    }

    // Used during the phases of the game where the player should be able to control their characters.
    public void EnableControl() {
        this.enabled = true;
        //m_CanvasGameObject.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Hazard")) {
            //Debug.Log("Hit the " + collision.name.ToString() + ".");
            /// do game over logic
            m_hasCollided = true;
            m_collidedInfo = collision.gameObject.GetComponent<ICollidable>().CollideMessage();
            /// Turn off collide message so if something else collides with the player character after game over, the name of what collided won't change.
            collision.gameObject.GetComponent<ICollidable>().Enabled = false;

            DisableControl();
            //print("You Died");
        }
    }

    //private void OnCollisionStay2D(Collision2D collision) {
    //    if (collision.otherCollider.CompareTag("Hazard")) {
    //        // do game over logic
    //        print("You Died");
    //    }
    //}

    private void SeagullDeath() {
        m_hasCollided = true;
        DisableControl();
        /// Instantiate Seagull and add Seagull's Animation
        //Instantiate(m_seagullPrefab); 
        //Instantiate();
        //print("You Died: Eaten By Seagull");
        m_gameLoopManager.m_CauseOfDeathText.text += "Eaten By A Seagull";
    }

    private void DeepSeaPressureDeath() {
        m_hasCollided = true;
        DisableControl();
        m_deepSeaPressureAlert.GetComponent<Image>().enabled = false;
        m_deepSeaPressureAlert.GetComponent<Animator>().enabled = false;

        //print("You Died: Too Much Sea Pressure");
        m_gameLoopManager.m_CauseOfDeathText.text += "The Deep Sea Pressure Was Too Strong";
    }

    public float GetDistance() {
        return Vector3.Distance(new Vector3(0.0f, 0.0f, 0.0f), transform.position);
    }

    public string GetDistanceString() {
        return GetDistance().ToString("F2");
    }

    /// Used during the phases of the game where the player shouldn't be able to control their characters.
    public void DisableControl() {
        this.enabled = false;
        //m_CanvasGameObject.SetActive(false);

        //this.m_ForwardSwimSpeed = 0.0f;
        CharacterRigidBody.transform.Translate(0.0f, 0.0f, 0.0f);
    }

    public void GetSpriteBounds()
    {
        m_Sprite = gameObject.GetComponentInParent<SpriteRenderer>(); //Set the reference to our SpriteRenderer component
        //m_sprite.bounds.extents.x == Distance to the right side, from your center point
        //-m_sprite.bounds.extents.x == Distance to the left side
        //m_sprite.bounds.extents.y == Distance to the top
        //-m_sprite.bounds.extents.y == Distance to the bottom
        Debug.Log("~~~~~~~~~~~~~~~~~m_sprite.bounds.extents = " + m_Sprite.bounds.extents + " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        m_RightExtent = m_Sprite.bounds.extents.x;
        m_LeftExtent = -m_Sprite.bounds.extents.x;
        m_UpExtent = m_Sprite.bounds.extents.y;
        m_DownExtent = -m_Sprite.bounds.extents.y;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PressEscToClose : MonoBehaviour {

    [SerializeField] private GameObject m_CreditsScrollView;

    private MenuManager m_menuManager;
    //[SerializeField] private MenuManager m_menuManager { get { return m_menuManager; } set { value = GetComponent<MenuManager>(); } }

    [SerializeField] private GameObject m_DefaultButton;

    //void GetDefaultButton() {
    //    m_DefaultButton = m_menuManager.m_DefaultButton;
    //}

    // Update is called once per frame
    void Update() {
        //GetDefaultButton();

        if (m_CreditsScrollView.activeInHierarchy && Input.GetButtonDown("Menu")) {
            m_CreditsScrollView.SetActive(false);

            ///// This makes sure that the UI button is selectable for controllers.
            ///// Ensures that the button has a select highlight by default.
            //EventSystem.current.SetSelectedGameObject(null);
            //EventSystem.current.firstSelectedGameObject = m_menuManager.gameObject;
            //m_menuManager.m_DefaultButton.GetComponent<Button>().Select();
        }
    }
}

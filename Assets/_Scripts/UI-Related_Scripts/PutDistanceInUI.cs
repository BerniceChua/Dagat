﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PutDistanceInUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_distanceText;

    [SerializeField] private PlayerMovement m_playerCharacter;

    // Start is called before the first frame update
    void Start()
    {
        //InvokeRepeating("UIShowDistance", 1.0f, 1.0f);
    }

    // Update is called once per frame
    void Update() {
        m_distanceText.text = m_playerCharacter.GetDistance().ToString("F2");
    }

    //void UIShowDistance() {
    //    m_distanceText.text = m_playerCharacter.GetDistance().ToString("F2");
    //}

}

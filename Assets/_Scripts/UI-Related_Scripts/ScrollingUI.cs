﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Source: https://www.youtube.com/watch?v=je1ZHOn3my4
/// </summary>
public class ScrollingUI : MonoBehaviour {

    [SerializeField] private ScrollingUIManager m_scrollingUIManager;

    // Update is called once per frame
    void Update() {
        this.gameObject.transform.Translate(m_scrollingUIManager.ScrollingDirection());
    }

}

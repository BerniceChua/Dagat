﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Source: 
/// 
/// https://www.youtube.com/watch?v=je1ZHOn3my4
/// 
/// https://www.youtube.com/watch?v=Oav9kbL5d3c
/// </summary>
public class ScrollingUIManager : MonoBehaviour {

    [SerializeField] private GameObject[] m_backgroundsTiles;

    public float m_scrollSpeedX;
    public float m_scrollSpeedY;

    [SerializeField] public enum ScrollDirection {
        LeftToRight, 
        RightToLeft, 
        UpToDown, 
        DownToUp, 
        DiagonalToLeftAndDown,
        DiagonalToRightAndDown,
        DiagonalToLeftAndUp,
        DiagonalToRightAndUp,
    };

    [SerializeField] public ScrollDirection m_scrollDirection;

    public Vector3 ScrollingDirection() {
        switch (m_scrollDirection) {
            case ScrollDirection.LeftToRight:
                return new Vector3(m_scrollSpeedX, 0.0f, 0.0f);
            case ScrollDirection.RightToLeft:
                return new Vector3(-1 * m_scrollSpeedX, 0.0f, 0.0f);
            case ScrollDirection.DownToUp:
                return new Vector3(0.0f, m_scrollSpeedY, 0.0f);
            case ScrollDirection.UpToDown:
                return new Vector3(0.0f,-1 * m_scrollSpeedY, 0.0f);
            case ScrollDirection.DiagonalToLeftAndDown:
                return new Vector3(-1 * m_scrollSpeedX, -1 * m_scrollSpeedY, 0.0f);
            case ScrollDirection.DiagonalToRightAndDown:
                return new Vector3(m_scrollSpeedX, -1 * m_scrollSpeedY, 0.0f);
            case ScrollDirection.DiagonalToLeftAndUp:
                return new Vector3(-1 * m_scrollSpeedX, m_scrollSpeedY, 0.0f);
            case ScrollDirection.DiagonalToRightAndUp:
                return new Vector3(m_scrollSpeedX, m_scrollSpeedY, 0.0f);
            default:
                return new Vector3(0.0f, 0.0f, 0.0f);
        }
    }

}

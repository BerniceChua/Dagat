﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Source: 
/// https://www.youtube.com/watch?v=E7gmylDS1C4
/// </summary>
public class DestroyOnScreenboundsExample : MonoBehaviour {

    public float speed = 10.0f;
    private Rigidbody2D rb2d;
    private Vector2 screenBounds;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = this.GetComponent<Rigidbody2D>();
        rb2d.velocity = new Vector2(-speed, 0.0f);
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < screenBounds.x * 2.0f) {
            Destroy(this.gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Source: 
/// https://www.youtube.com/watch?v=E7gmylDS1C4
/// </summary>
public class DeployOnScreenboundsExample : MonoBehaviour
{
    public GameObject gameObjPrefab;
    public float respawnTime = 1.0f;
    private Vector2 screenBounds;

    // Start is called before the first frame update
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

        StartCoroutine(HazardsWave());
    }

    private void SpawnGameObj() {
        GameObject thing = Instantiate(gameObjPrefab) as GameObject;
        thing.transform.position = new Vector2(screenBounds.x * -2.0f, Random.Range(-screenBounds.y, screenBounds.y) );
    }

    private IEnumerator HazardsWave() {
        while (true) {
            yield return new WaitForSeconds(respawnTime);
            SpawnGameObj();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Source: https://www.youtube.com/watch?v=t2Cs71rDlUg
/// </summary>
public class NoOverlapSpawnerExample : MonoBehaviour
{
    public GameObject ball;
    public Collider2D[] colliders;
    public float radius;

    public LayerMask m_LayerMask;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        colliders = Physics2D.OverlapCircleAll(transform.position, radius);
    }

    public void SpawnBall() {

        Vector2 spawnPosition = new Vector2(0.0f, 0.0f);
        bool canSpawnHere = false;

        int safetyNet = 0;

        while (!canSpawnHere) {
            float spawnPosX = Random.Range(-8.5f, 9.5f);
            float spawnPosY = Random.Range(-4.5f, 5.5f);

            spawnPosition = new Vector2(spawnPosX, spawnPosY);
            canSpawnHere = PreventSpawnOverlap(spawnPosition);

            if (canSpawnHere) {
                break;
            }

            safetyNet++;
            if (safetyNet > 50) {
                Debug.Log("Too many attempts to spawn.");
                break;
            }
        }

        GameObject newBall = Instantiate(ball, spawnPosition, Quaternion.identity) as GameObject;
        
        /// My experiment: This line below is what allows the spawned objects to not spawn over themselves?
        Collider2D spawnedObjectCollider = Physics2D.OverlapCircle(newBall.transform.position, radius, m_LayerMask);

    }

    private bool PreventSpawnOverlap(Vector2 spawnPos) {
        colliders = Physics2D.OverlapCircleAll(transform.position, radius, m_LayerMask);

        for (int i = 0; i < colliders.Length; i++) {
            Vector2 centerPoint = colliders[i].bounds.center;
            float width = colliders[i].bounds.extents.x;
            float height = colliders[i].bounds.extents.y;

            float leftExtent = centerPoint.x - width;
            float rightExtent = centerPoint.x + width;
            float lowerExtent = centerPoint.y - height;
            float upperExtent = centerPoint.y + height;

            /// Check if the spawning position is inside the extents of the colliders of things that we want to avoid overlapping.
            /// If it's inside the colliders, can't spawn here (false).
            /// If it's outside the colliders, can spawn here (true).
            if (spawnPos.x >= leftExtent && spawnPos.x <= rightExtent) {
                if (spawnPos.y >= lowerExtent && spawnPos.y <= upperExtent) {
                    return false;
                }
            }
        }
        return true;

    }

}

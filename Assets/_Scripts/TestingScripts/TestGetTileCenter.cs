﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TestGetTileCenter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Tilemap m_tilemap = transform.parent.GetComponent<Tilemap>();
        Vector3Int m_cellPosition = m_tilemap.WorldToCell(transform.position);
        transform.position = m_tilemap.GetCellCenterWorld(m_cellPosition);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// https://answers.unity.com/questions/40996/check-if-position-is-inside-a-collider.html
/// </summary>
public class TestCreateHexTileMap2 : MonoBehaviour
{
    public GameObject hexPrefab;

    [HideInInspector] public List<Vector2> m_PossibleV2Positions = new List<Vector2>();

    // Size of the map in terms of number of hex tiles
    // This is NOT representative of the amount of 
    // world space that we're going to take up.
    // (i.e. our tiles might be more or less than 1 Unity World Unit)
    [SerializeField] private int m_width = 4;
    [SerializeField] private int m_height = 3;

    //[SerializeField] private float xOffset = 0.882f;
    //[SerializeField] private float yOffset = 0.764f;
    [SerializeField] private float m_xOffset = 2.5f;
    [SerializeField] private float m_yOffset = 2.17f;
    [SerializeField] public float m_OffsetMultiplier = 5.0f;
    [SerializeField] public float m_OffsetShrinkRate = 0.2f;

    /// <summary>
    /// Internally sets the starting position based on the screen width & height.
    /// </summary>
    private float m_startXPos;
    private float m_startYPos;
    private Vector2 m_screenBoundary;
    private Vector2 m_screenBottomRight;
    private Vector2 m_screenBottomLeft;


    // Use this for initialization
    void Start()
    {

        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        m_screenBottomRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, Camera.main.transform.position.z));
        m_screenBottomLeft = Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z));

        m_startXPos = m_screenBottomRight.x;
        m_startYPos = m_screenBottomRight.y;

        CreateHexTiles();

    }

    public List<Vector2> CreateHexTiles() {
        for (int x = 0; x < m_width; x++) {
            for (int y = 0; y < m_height; y++) {

                //float xPos = x * m_xOffset;
                //float xPos = m_startXPos + (x * m_xOffset);
                //float xPos = m_startXPos;
                float xPos = (m_startXPos / 4) + (x * m_xOffset);
                float yPos = (m_startYPos / 2.3f) + (y * m_yOffset);

                // Are we on an odd row?
                if (y % 2 == 1) {
                    xPos += m_xOffset / 2f;
                }

                m_PossibleV2Positions.Add(new Vector2(xPos * m_OffsetMultiplier, yPos * m_OffsetMultiplier));

                GameObject hex_go = (GameObject)Instantiate(hexPrefab, new Vector3(xPos * m_OffsetMultiplier, yPos * m_OffsetMultiplier, 2.0f), Quaternion.identity);

                // Name the gameobject something sensible.
                hex_go.name = "Hex_" + x + "_" + y;

                // Make sure the hex is aware of its place on the map
                hex_go.GetComponent<Hex2D>().x = x;
                hex_go.GetComponent<Hex2D>().y = y;

                // For a cleaner hierachy, parent this hex to the map
                hex_go.transform.SetParent(this.transform);

            }
        }
        Debug.Log("m_PossibleV2Positions.Length = " + m_PossibleV2Positions.Count);
        foreach (Vector2 v2Pos in m_PossibleV2Positions)
        {
            Debug.Log(v2Pos);
        }
        return m_PossibleV2Positions;
    }

    //// Start is called before the first frame update
    //void Start()
    //{

    //}

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ShrinkOffsetMultiplier() {
        m_OffsetMultiplier = m_OffsetMultiplier * (1.0f - m_OffsetShrinkRate);
    }
}

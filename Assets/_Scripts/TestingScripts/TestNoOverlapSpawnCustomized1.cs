﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Source: https://forum.unity.com/threads/prevent-instantiation-on-top-of-other-objects.410845/#post-2701554
/// </summary>
public class TestNoOverlapSpawnCustomized1 : MonoBehaviour {

    [SerializeField] private int m_beginningMargin;
    [SerializeField] private int m_marginDecreasePerInterval;
    private int m_currentMargin;

    [SerializeField] private GameObject[] m_hazardsPrefabs;
    [SerializeField] private Collider2D[] m_colliders;
    [SerializeField] private float m_radius;
    [SerializeField] private LayerMask m_layerMaskCollisionTest;


    [SerializeField] private float m_randomStartingXMin = -2;
    [SerializeField] private float m_randomStartingXMax = 5;
    [SerializeField] private float m_randomYMin = -6;
    [SerializeField] private float m_randomYMax = 6;

    private Vector2 m_screenBoundary;


    // Start is called before the first frame update
    void Start()
    {
        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

        m_currentMargin = m_beginningMargin;

        for (int i = 0; i < m_hazardsPrefabs.Length; i++) {
            SpawnTheFishies(m_hazardsPrefabs[i]);
        }
    }

    // Update is called once per frame
    //void Update()
    //{
        
    //}

    private Vector2 GetValidSpawnLocation(GameObject hazardsPrefab) {
        /// Creates a variable called objectCollider and assigns the 2D Collider Component from the Game Object that is being passed into the Method.
        Collider2D objCollider = hazardsPrefab.GetComponent<Collider2D>();

        /// Sets the new postion to (0,0,0). Changed to Vector 3 3 in my code because of the objectCollider.bounds.extents which is a Vector 3
        Vector3 newPosition = Vector2.zero;

        /// Boolian for getting out of the Do/While loop
        bool validPosition = false;

        /// failsafe to prevent infinite loops
        int failureLimit = 100;
        int fails = 0;

        do {
            // get the bottom left corner of the screen in world space
            Vector2 screenMin = Camera.main.ViewportToWorldPoint(Vector3.zero); // Vector3.zero = same as new Vector3(0,0,0)
            // get the top right corner of the screen in world space
            Vector2 screenMax = Camera.main.ViewportToWorldPoint(Vector3.one); // Vector3.one = same as new Vector3(1,1,1)

            // get a random position for the X and Y positional coordanintes on screen
            newPosition.x = Random.Range(screenMin.x, screenMax.x);
            newPosition.y = Random.Range(screenMin.y, screenMax.y);

            // these next two values are passed to the "OverlapAreaAll" function to tell it
            // what area to check for collisions.  They are opposite corners of a square.

            // we add and subtract the object collider's half-width (extents) from the new position
            // to get the opposing corners of the square area that the object will take up
            // at the new position
            Vector3 min = ( (newPosition ) - (objCollider.bounds.extents * m_radius) );
            Vector3 max = ( (newPosition ) + (objCollider.bounds.extents * m_radius) );
            Debug.Log("min = " + min);
            Debug.Log("max = " + max);

            // !!!!!!!!!!!!!!!!CHECKS FOR OVERLAPING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            Collider2D[] overlapObjects = Physics2D.OverlapAreaAll(min, max, m_layerMaskCollisionTest);

            //if it does not overlap any objects in the layermask

            if (overlapObjects.Length == 0) {
                Debug.Log("good");

                //break out of the Do/While loop
                validPosition = true;

                // reset the failsafe
                fails = 0;
            } else {
                Debug.Log("Overlapping has occured");

                fails++;
            }

            // added a fail-safe to prevent infinite looping if there is no space left
        } while (!validPosition && fails < failureLimit);

        return newPosition;
    }

    public void SpawnTheFishies(GameObject thisHazardsPrefab) {
        /// Spawns my object off screen
        GameObject newObject = (GameObject)Instantiate(thisHazardsPrefab, new Vector3(-100, -100, -100), Quaternion.identity); //Keep this code, changed spawnedObject to prefabToSpawn for this test

        /// Choose a position that doesn't overlap on the specified layermask
        Vector2 randomSpawnPosition = GetValidSpawnLocation(newObject);
        if (randomSpawnPosition == Vector2.zero) {
            Debug.Log("no more room!");
            Destroy(newObject);
        } else {
            newObject.transform.position = randomSpawnPosition;
        }

        Debug.Log("newObject.transform.position = " + newObject.transform.position);
    }

}

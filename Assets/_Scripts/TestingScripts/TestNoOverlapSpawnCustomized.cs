﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestNoOverlapSpawnCustomized : MonoBehaviour {

    [SerializeField] private int m_beginningMargin;
    [SerializeField] private int m_marginDecreasePerInterval;
    private int m_currentMargin;

    [SerializeField] private GameObject[] m_hazardsPrefabs;
    [SerializeField] private Collider2D[] m_colliders;
    [SerializeField] private float m_radius;
    [SerializeField] private LayerMask m_layerMask;


    [SerializeField] private float m_randomStartingXMin = -2;
    [SerializeField] private float m_randomStartingXMax = 5;
    [SerializeField] private float m_randomYMin = -6;
    [SerializeField] private float m_randomYMax = 6;

    private Vector2 m_screenBoundary;


    // Start is called before the first frame update
    void Start()
    {
        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

        m_currentMargin = m_beginningMargin;

        for (int i = 0; i < m_hazardsPrefabs.Length; i++) {
            SpawnTheFishies(m_hazardsPrefabs[i]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        m_colliders = Physics2D.OverlapCircleAll(transform.position, m_radius);
    }

    public void SpawnTheFishies(GameObject thisHazardsPrefab) {

        Collider2D thisCollider = thisHazardsPrefab.GetComponent<Collider2D>();

        ///
        //Bounds thisBoundary = thisCollider.bounds;

        //Vector2 centerPoint = thisBoundary.center;
        //float width = thisBoundary.extents.x;
        //float height = thisBoundary.extents.y;

        //float leftExtent = centerPoint.x - width;
        //float rightExtent = centerPoint.x + width;
        //float lowerExtent = centerPoint.y - height;
        //float upperExtent = centerPoint.y + height;
        ///
        Extents2D myExtents = new Extents2D();
        myExtents = myExtents.Find2DExtents(thisCollider);

        Vector2 spawnPos = new Vector2(0.0f, 0.0f);
        bool canSpawnHere = false;
        int safetyNet = 0;

        while (canSpawnHere == false) {
            //float spawnPosX = Random.Range(-8.5f, 9.5f);
            //float spawnPosY = Random.Range(-4.5f, 5.5f);
            float spawnPosX = Random.Range(m_screenBoundary.x + m_randomStartingXMin, m_screenBoundary.x + m_randomStartingXMax);
            float spawnPosY = Random.Range(m_randomYMin, m_randomYMax);

            spawnPos = new Vector3(spawnPosX, spawnPosY);
            canSpawnHere = PreventSpawnOverlap(spawnPos);

            if (canSpawnHere) {
                break;
            }

            safetyNet++;
            if (safetyNet > 50) {
                Debug.Log("Too many attempts.");
                break;
            }
        }
        GameObject thisGameObj = Instantiate(thisHazardsPrefab, spawnPos, Quaternion.identity) as GameObject;
    }

    bool PreventSpawnOverlap(Vector2 spawnPos) {
        m_colliders = Physics2D.OverlapCircleAll(transform.position, m_radius, m_layerMask);

        /// Check bounds of each collider, and return false if the intended spawn position is within the bounds of these colliders.
        for (int i = 0; i < m_colliders.Length; i++) {
            //Vector2 centerPoint = m_colliders[i].bounds.center;
            //float width = m_colliders[i].bounds.extents.x;
            //float height = m_colliders[i].bounds.extents.y;

            //float leftExtent = centerPoint.x - width;
            //float rightExtent = centerPoint.x + width;
            //float lowerExtent = centerPoint.y - height;
            //float upperExtent = centerPoint.y + height;

            Extents2D myExtents = new Extents2D();

            myExtents = myExtents.Find2DExtents(m_colliders[i]);

            if ( (spawnPos.x + myExtents.rightExtent + m_currentMargin) >= myExtents.leftExtent && (spawnPos.x - myExtents.leftExtent - m_currentMargin) <= myExtents.rightExtent) {
                if ( (spawnPos.y + myExtents.upExtent + m_currentMargin) >= myExtents.downExtent && (spawnPos.y - myExtents.downExtent - m_currentMargin) <= myExtents.upExtent) {
                    return false;
                }
            }
        }
        return true;

    }

}

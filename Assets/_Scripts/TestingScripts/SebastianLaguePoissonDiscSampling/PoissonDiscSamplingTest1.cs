﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoissonDiscSamplingTest1 : MonoBehaviour {

    public float m_RadiusOfPadding = 1;
    public Vector2 m_SampleRegionStart;
    public Vector2 m_SampleRegionEnd;
    public Vector2 m_RegionSize = Vector2.one;
    public int m_RejectionSamples = 30;
    public float m_DisplayRadius = 1;

    private List<Vector2> m_points;


    private Vector2 m_screenBoundMin;
    private Vector2 m_screenBoundary;


    // Start is called before the first frame update
    void Start() {
        m_screenBoundMin = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f - (Screen.height / 2), 0.0f));

        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

        Debug.Log("m_screenBoundMin = " + m_screenBoundMin);
        Debug.Log("m_screenBoundary = " + m_screenBoundary);
    }

    // Update is called once per frame
    void Update() {
        m_screenBoundMin = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f - (Screen.height / 2), 0.0f));
        m_SampleRegionStart = m_screenBoundMin;

        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 2, Screen.height + (Screen.height / 2), 0.0f));
        m_SampleRegionEnd = m_screenBoundary;

        m_RegionSize = (m_SampleRegionEnd - m_SampleRegionStart);
        //m_RegionSize = (m_SampleRegionEnd - m_SampleRegionStart) / 2;
        //float xLength = Vector2.Distance(m_SampleRegionEnd, new Vector2((m_SampleRegionEnd.x - m_SampleRegionStart.x), m_SampleRegionEnd.y));
        //float yLength = Vector2.Distance(m_SampleRegionEnd, new Vector2(m_SampleRegionEnd.x, (m_SampleRegionEnd.y - m_SampleRegionStart.y)));
        //Debug.Log("xLength = " + xLength);
        //Debug.Log("yLength = " + yLength);
        ////Vector2 sampleRegionSize = new Vector2(xLength, yLength);
        ////Debug.Log("sampleRegionSize = " + sampleRegionSize);
        //m_RegionSize = new Vector2(xLength, yLength);
        Debug.Log("m_RegionSize (inside PoissonDiscSamplingTest1.Update() = " + m_RegionSize);

        m_points = PoissonDiscSampling1.GeneratePoints(m_RadiusOfPadding, m_SampleRegionStart, m_SampleRegionEnd, m_RejectionSamples);

    }

    private void OnValidate() {
        //m_RegionSize = (m_SampleRegionEnd - m_SampleRegionStart);

        m_points = PoissonDiscSampling1.GeneratePoints(m_RadiusOfPadding, m_SampleRegionStart, m_SampleRegionEnd, m_RejectionSamples);
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(m_screenBoundMin, 1);

        Gizmos.DrawWireCube(m_screenBoundary, Vector3.one);

        Vector2 centerOfRegion = (m_SampleRegionEnd + m_SampleRegionStart) / 2;
        Debug.Log("centerOfRegion = " + centerOfRegion);
        //m_RegionSize = m_SampleRegionEnd - m_SampleRegionStart;
        //m_RegionSize = (m_SampleRegionEnd - m_SampleRegionStart) / 2;
        //m_RegionSize = m_SampleRegionEnd + m_SampleRegionStart;
        Debug.Log("m_RegionSize (inside OnDrawGizmos() ) = " + m_RegionSize);
        Gizmos.DrawWireSphere(centerOfRegion, 1);

        //Gizmos.DrawWireCube(m_RegionSize/2, m_SampleRegionEnd - m_SampleRegionStart);
        //Gizmos.DrawWireCube(m_RegionSize, (m_SampleRegionEnd - m_SampleRegionStart) / 2);
        Gizmos.DrawWireCube(centerOfRegion, m_RegionSize);
        if (m_points!= null) {
            foreach (Vector2 point in m_points) {
                Gizmos.DrawSphere(point, m_DisplayRadius);
            }
        }

        m_points = PoissonDiscSampling1.GeneratePoints(m_RadiusOfPadding, m_SampleRegionStart, m_SampleRegionEnd, m_RejectionSamples);
    }

    //private void RecycleHazards(GameObject hazard) {
    //    if (m_points != null)
    //    {
    //        foreach (Vector2 point in m_points)
    //        {
    //            Gizmos.DrawSphere(point, m_DisplayRadius);
    //        }
    //    }
    //}
}

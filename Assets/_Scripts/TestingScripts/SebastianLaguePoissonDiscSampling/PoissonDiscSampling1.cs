﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public class PoissonDiscSampling : MonoBehaviour
//{
//    // Start is called before the first frame update
//    void Start()
//    {
        
//    }

//    // Update is called once per frame
//    void Update()
//    {
        
//    }
//}

public static class PoissonDiscSampling1 {
    public static List<Vector2> GeneratePoints(float radius, Vector2 sampleRegionSize, int numSamplesBeforeRejection) {
        /// cellSize comes from Pythagorean Theorem: 
        /// `c^2 = a^2 + b^2`
        /// `c` is the hypothenus, and `a` & `b` are the 2 sizes.
        /// The hypothenus is the radius of the circle.
        /// The cellSize is the side of a square in the grid.
        float cellSize = radius / Mathf.Sqrt(2);
        
        /// grid is the # of cells on the x & y axes.
        /// It's the # of times that the cellsize fits into the sampleRegionSize.
        //int[,] grid = new int[Mathf.CeilToInt(sampleRegionSize.x / cellSize), Mathf.CeilToInt(sampleRegionSize.y / cellSize)];
        int[,] grid = new int[Mathf.FloorToInt(sampleRegionSize.x / cellSize), Mathf.CeilToInt(sampleRegionSize.y / cellSize)];
        List<Vector2> points = new List<Vector2>();
        List<Vector2> spawnPoints = new List<Vector2>();

        spawnPoints.Add(sampleRegionSize / 2);
        while (spawnPoints.Count > 0) {
            int spawnIndex = Random.Range(0, spawnPoints.Count);
            Vector2 spawnCenter = spawnPoints[spawnIndex];

            bool candidateAccepted = false;

            /// find a random point around spawnCenter
            /// if we cannot find a good place to put this point, then remove this spawnPoint from the list.
            for (int i = 0; i < numSamplesBeforeRejection; i++) {
                float angle = Random.value * Mathf.PI*2;
                Vector2 direction = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle) );
                Vector2 candidate = spawnCenter + direction * Random.Range(radius, 2*radius);
                Debug.Log("PoissonDiscSampling.cs candicate = " + candidate);

                if (IsValid(candidate, sampleRegionSize, cellSize, radius, points, grid)) {
                    points.Add(candidate);
                    spawnPoints.Add(candidate);
                    grid[(int)(candidate.x / cellSize), (int)(candidate.y / cellSize)] = points.Count;
                    candidateAccepted = true;
                    break;
                }
            }
            /// Remove this spawnPoiint from the candidates.
            if (!candidateAccepted) {
                spawnPoints.RemoveAt(spawnIndex);
            }
        }

        return points;
    }

    private static bool IsValid(Vector2 myCandidate, Vector2 mySampleRegionSize, float myCellSize, float myRadius, List<Vector2> myPoints, int[,] myGrid) {
        //if (myCandidate.x >= 0 && myCandidate.x < mySampleRegionSize.x && myCandidate.y >= 0 && myCandidate.y < mySampleRegionSize.y) {
        bool xBetweenZeroAndSampleRegionSize = myCandidate.x >= 0 && myCandidate.x < mySampleRegionSize.x;
        bool yBetweenZeroAndSampleRegionSize = myCandidate.y >= 0 && myCandidate.y < mySampleRegionSize.y;
        if (xBetweenZeroAndSampleRegionSize && yBetweenZeroAndSampleRegionSize) {
            int cellX = (int)(myCandidate.x / myCellSize);
            int cellY = (int)(myCandidate.y / myCellSize);

            int searchStartX = Mathf.Max(0, cellX - 2);
            int searchEndX = Mathf.Min(cellX + 2, myGrid.GetLength(0)-1);

            int searchStartY = Mathf.Max(0, cellY - 2);
            int searchEndY = Mathf.Min(cellY + 2, myGrid.GetLength(1)-1);

            for (int x = searchStartX; x <= searchEndX; x++) {
                for (int y = searchStartY; y <= searchEndY; y++) {
                    int pointIndex = myGrid[x, y] - 1;
                    if (pointIndex != -1) {
                        float squareDistance = (myCandidate - myPoints[pointIndex]).sqrMagnitude;
                        if (squareDistance < myRadius*myRadius) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    public static List<Vector2> GeneratePoints(float radius, Vector2 sampleRegionStart, Vector2 sampleRegionEnd, int numSamplesBeforeRejection) {
        //Vector2 sampleRegionSize = sampleRegionEnd - sampleRegionStart;
        //Vector2 sampleRegionSize = (sampleRegionEnd + sampleRegionStart) / 2;

        /// region size is area of a quadrilateral or square.
        /// area of a quadrilateral is length * width
        /// lenght of 2 vectors is their dot products
        float xLength = Vector2.Distance(sampleRegionEnd, new Vector2((sampleRegionEnd.x - sampleRegionStart.x), sampleRegionEnd.y));
        float yLength = Vector2.Distance(sampleRegionEnd, new Vector2(sampleRegionEnd.x, (sampleRegionEnd.y - sampleRegionStart.y)));
        Debug.Log("xLength = " + xLength);
        Debug.Log("yLength = " + yLength);
        Vector2 sampleRegionSize = new Vector2 (xLength, yLength);
        Debug.Log("sampleRegionSize = " + sampleRegionSize);
        //Vector2 sampleRegionSize = sampleRegionEnd;

        /// cellSize comes from Pythagorean Theorem: 
        /// `c^2 = a^2 + b^2`
        /// `c` is the hypothenus, and `a` & `b` are the 2 sizes.
        /// The hypothenus is the radius of the circle.
        /// The cellSize is the side of a square in the grid.
        float cellSize = radius / Mathf.Sqrt(2);

        Debug.Log("radius = " + radius);
        Debug.Log("cellSize = " + cellSize);
        Debug.Log("sampleRegionSize.x = " + sampleRegionSize.x);
        Debug.Log("sampleRegionSize.y = " + sampleRegionSize.y);
        /// grid is the # of cells on the x & y axes.
        /// It's the # of times that the cellsize fits into the sampleRegionSize.
        int[,] grid = new int[Mathf.CeilToInt(sampleRegionSize.x / cellSize), Mathf.CeilToInt(sampleRegionSize.y / cellSize)];
        //int[,] grid = new int[Mathf.CeilToInt( (sampleRegionEnd.x - sampleRegionStart.x) / cellSize), Mathf.CeilToInt( (sampleRegionEnd.y - sampleRegionStart.y) / cellSize)];
        //Debug.Log("grid.Length = " + grid.Length);
        //Debug.Log("grid = ");
        //for (int i = 0; i < grid.Length; i++)
        //{
        //    for (int j = 0; j < grid.GetLength(1); j++)
        //    {
        //        Debug.Log("grid[" + i + "," + j + "] = " + grid[i,j]);
        //    }
        //}
        List<Vector2> points = new List<Vector2>();
        List<Vector2> spawnPoints = new List<Vector2>();

        spawnPoints.Add(sampleRegionSize / 2);
        while (spawnPoints.Count > 0)
        {
            int spawnIndex = Random.Range(0, spawnPoints.Count);
            Vector2 spawnCenter = spawnPoints[spawnIndex];

            bool candidateAccepted = false;

            /// find a random point around spawnCenter
            /// if we cannot find a good place to put this point, then remove this spawnPoint from the list.
            for (int i = 0; i < numSamplesBeforeRejection; i++) {
                float angle = Random.value * Mathf.PI * 2;
                Vector2 direction = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
                Vector2 candidate = spawnCenter + direction * Random.Range(radius, 2 * radius);

                if (IsValid(candidate, sampleRegionStart, sampleRegionEnd, cellSize, radius, points, grid))
                {
                    Debug.Log("Inside if ( IsValid(...) ) AKA IsValid(...) is true.");
                    points.Add(candidate);
                    spawnPoints.Add(candidate);
                    grid[(int)(candidate.x / cellSize), (int)(candidate.y / cellSize)] = points.Count;
                    Debug.Log("grid[(int)(" + candidate.x  + "/" + cellSize + "), (int)(" + candidate.y + "/" + cellSize + ")] = " + grid[(int)(candidate.x / cellSize), (int)(candidate.y / cellSize)]);
                    candidateAccepted = true;
                    break;
                }
                Debug.Log("grid.Length = " + grid.Length);
                Debug.Log("grid = ");
                for (int x = 0; x < grid.Length; x++)
                {
                    for (int y = 0; y < grid.GetLength(1); y++)
                    {
                        Debug.Log("grid[" + x + "," + y + "] = " + grid[x, y]);
                    }
                }
            }
            /// Remove this spawnPoiint from the candidates.
            if (!candidateAccepted) {
                spawnPoints.RemoveAt(spawnIndex);
            }
        }

        return points;
    }
    private static bool IsValid(Vector2 myCandidate, Vector2 mySampleRegionStart, Vector2 mySampleRegionEnd, float myCellSize, float myRadius, List<Vector2> myPoints, int[,] myGrid)
    {
        //if (myCandidate.x >= mySampleRegionStart.x && myCandidate.x < mySampleRegionSize.x && myCandidate.y >= mySampleRegionStart.y && myCandidate.y < mySampleRegionSize.y) {
        bool xInsideSampleRegionSize = myCandidate.x >= mySampleRegionStart.x && myCandidate.x < mySampleRegionEnd.x;
        bool yInsideSampleRegionSize = myCandidate.y >= mySampleRegionStart.y && myCandidate.y < mySampleRegionEnd.y;
        if (xInsideSampleRegionSize && yInsideSampleRegionSize)
        {
            int cellX = (int)(myCandidate.x / myCellSize);
            int cellY = (int)(myCandidate.y / myCellSize);

            int searchStartX = Mathf.Max(0, cellX - 2);
            int searchEndX = Mathf.Min(cellX + 2, myGrid.GetLength(0) - 1);

            int searchStartY = Mathf.Max(0, cellY - 2);
            int searchEndY = Mathf.Min(cellY + 2, myGrid.GetLength(1) - 1);

            for (int x = searchStartX; x <= searchEndX; x++)
            {
                for (int y = searchStartY; y <= searchEndY; y++)
                {
                    int pointIndex = myGrid[x, y] - 1;
                    if (pointIndex != -1)
                    {
                        float squareDistance = (myCandidate - myPoints[pointIndex]).sqrMagnitude;
                        if (squareDistance < myRadius * myRadius)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

}

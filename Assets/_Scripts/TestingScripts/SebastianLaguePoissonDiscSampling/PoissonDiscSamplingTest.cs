﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoissonDiscSamplingTest : MonoBehaviour {

    public float m_Radius = 1;
    public Vector2 m_RegionSize = Vector2.one;
    public int m_RejectionSamples = 30;
    public float m_DisplayRadius = 1;

    private List<Vector2> m_points;

    // Start is called before the first frame update
    void Start()
    {
        m_RegionSize = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));
    }

    //// Update is called once per frame
    //void Update() {

    //}

    void OnValidate() {
        m_points = PoissonDiscSampling.GeneratePoints(m_Radius, m_RegionSize, m_RejectionSamples);
    }

    void OnDrawGizmos() {
        Gizmos.DrawWireCube(m_RegionSize/2, m_RegionSize);
        if (m_points!= null) {
            foreach (Vector2 point in m_points) {
                Gizmos.DrawSphere(point, m_DisplayRadius);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScreenToWorldPoint : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("screen boundary max is upper right corner = " + Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z)) );
        Debug.Log("screen boundary min (0,0,0) is the lower left corner = " + Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z)));
        Debug.Log("screen boundary negative = " + Camera.main.ScreenToWorldPoint(new Vector3(-Screen.width, -Screen.height, Camera.main.transform.position.z)));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0)) {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Debug.DrawRay(mousePos, Camera.main.transform.forward * 1000, Color.green);
            Debug.Log(mousePos);
        }
    }
}

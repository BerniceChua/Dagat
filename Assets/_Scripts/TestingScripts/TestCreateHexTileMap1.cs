﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCreateHexTileMap1 : MonoBehaviour
{
    public GameObject hexPrefab;

    // Size of the map in terms of number of hex tiles
    // This is NOT representative of the amount of 
    // world space that we're going to take up.
    // (i.e. our tiles might be more or less than 1 Unity World Unit)
    int width = 64;
    int height = 40;

    //[SerializeField] private float xOffset = 0.882f;
    //[SerializeField] private float yOffset = 0.764f;
    [SerializeField] private float xOffset = 2.5f;
    [SerializeField] private float yOffset = 2.17f;
    [SerializeField] public float m_OffsetMultiplier = 5.0f;
    [SerializeField] public float m_offsetShrinkRate = 0.2f;

    // Use this for initialization
    void Start() {

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {

                float xPos = x * xOffset;

                // Are we on an odd row?
                if (y % 2 == 1) {
                    xPos += xOffset / 2f;
                }

                GameObject hex_go = (GameObject)Instantiate(hexPrefab, new Vector3(xPos * m_OffsetMultiplier, y * yOffset * m_OffsetMultiplier, 2.0f), Quaternion.identity);

                // Name the gameobject something sensible.
                hex_go.name = "Hex_" + x + "_" + y;

                // Make sure the hex is aware of its place on the map
                hex_go.GetComponent<Hex2D>().x = x;
                hex_go.GetComponent<Hex2D>().y = y;

                // For a cleaner hierachy, parent this hex to the map
                hex_go.transform.SetParent(this.transform);

                // TODO: Quill needs to explain different optimization later...
                //hex_go.isStatic = true;

            }
        }

    }

    //// Start is called before the first frame update
    //void Start()
    //{
        
    //}

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ShrinkOffsetMultiplier() {
        m_OffsetMultiplier = m_OffsetMultiplier * (1.0f - m_offsetShrinkRate);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestNoOverlapSpawn : MonoBehaviour {

    [SerializeField] private GameObject[] m_hazardsPrefabs;
    [SerializeField] private Collider2D[] m_colliders;
    [SerializeField] private float m_radius;
    [SerializeField] private LayerMask m_layerMask;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < m_hazardsPrefabs.Length; i++) {
            SpawnTheFishies(m_hazardsPrefabs[i]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        m_colliders = Physics2D.OverlapCircleAll(transform.position, m_radius);
    }

    public void SpawnTheFishies(GameObject thisHazardsPrefab) {
        Vector2 spawnPos = new Vector2(0.0f, 0.0f);
        bool canSpawnHere = false;
        int safetyNet = 0;

        while (canSpawnHere == false) {
            float spawnPosX = Random.Range(-8.5f, 9.5f);
            float spawnPosY = Random.Range(-4.5f, 5.5f);

            spawnPos = new Vector3(spawnPosX, spawnPosY);
            canSpawnHere = PreventSpawnOverlap(spawnPos);

            if (canSpawnHere) {
                break;
            }

            safetyNet++;
            if (safetyNet > 50) {
                Debug.Log("Too many attempts.");
                break;
            }
        }
        GameObject thisGameObj = Instantiate(thisHazardsPrefab, spawnPos, Quaternion.identity) as GameObject;
    }

    bool PreventSpawnOverlap(Vector2 spawnPos) {
        m_colliders = Physics2D.OverlapCircleAll(transform.position, m_radius, m_layerMask);

        /// Check bounds of each collider, and return false if the intended spawn position is within the bounds of these colliders.
        for (int i = 0; i < m_colliders.Length; i++) {
            Vector2 centerPoint = m_colliders[i].bounds.center;
            float width = m_colliders[i].bounds.extents.x;
            float height = m_colliders[i].bounds.extents.y;

            float leftExtent = centerPoint.x - width;
            float rightExtent = centerPoint.x + width;
            float lowerExtent = centerPoint.y - height;
            float upperExtent = centerPoint.y + height;

            if (spawnPos.x >= leftExtent && spawnPos.x <= rightExtent) {
                if (spawnPos.y >= lowerExtent && spawnPos.y <= upperExtent) {
                    return false;
                }
            }
        }
        return true;

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastAlignerNoOverlap : MonoBehaviour {
    public GameObject[] itemsToPickFrom;
    public float raycastDistance = 100.0f;
    public float overlapTestBoxSize = 1.0f;
    public LayerMask spawnedObjectLayer;
    
    /// <summary>
    /// Put this outside the function so it's not making a new array each time.
    /// </summary>
    private Collider[] collidersInsideOverlapBox = new Collider[1];

    // Start is called before the first frame update
    void Start() {
        PositionRayast();
    }

    // Update is called once per frame
    void Update() {
        
    }

    private void PositionRayast() {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, Vector3.down, out hit, raycastDistance)) {

            Quaternion spawnRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

            Vector3 overlapTestBoxScale = new Vector3(overlapTestBoxSize, overlapTestBoxSize, overlapTestBoxSize);
            /// Where line 11 originally was.
            //Collider[] collidersInsideOverlapBox = new Collider[1];
            int numberOfCollidersFound = Physics.OverlapBoxNonAlloc(hit.point, overlapTestBoxScale, collidersInsideOverlapBox, spawnRotation, spawnedObjectLayer);

            Debug.Log("Number Of Colliders Found = " + numberOfCollidersFound);

            if (numberOfCollidersFound == 0) {
                //Debug.Log("Spawned the game object");
                Pick(hit.point, spawnRotation);
            } else {
                Debug.Log("Name Of Collider 0 Found = " + collidersInsideOverlapBox[0].name);
            }
        }
    }

    private void Pick(Vector3 positionToSpawn, Quaternion rotationToSpawn) {
        Debug.Log("Inside Pick(Vector3 positionToSpawn, Quaternion rotationToSpawn)");
        int randomIndex = Random.Range(0, itemsToPickFrom.Length);
        GameObject clone = Instantiate(itemsToPickFrom[randomIndex], positionToSpawn, rotationToSpawn);
    }
}

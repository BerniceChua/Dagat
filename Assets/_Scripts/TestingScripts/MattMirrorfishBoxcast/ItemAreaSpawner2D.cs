﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAreaSpawner2D : MonoBehaviour {

    public GameObject itemToSpread;
    public int numItemsToSpawn = 10;

    public float itemXSpread = 10.0f;
    public float itemYSpread = 0.0f;
    public float itemZSpread = 10.0f;

    private Vector2 m_screenBoundMin;
    private Vector2 m_screenBoundary;


    // Start is called before the first frame update
    void Start() {
        //SpreadItem();

        for (int i = 0; i < numItemsToSpawn; i++) {
            SpreadItem();
        }
    }

    //// Update is called once per frame
    //void Update() {
        
    //}

    private void SpreadItem() {
        m_screenBoundMin = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f - (Screen.height / 2), 0.0f));
        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 2, Screen.height + (Screen.height / 2), 0.0f));

        //Vector3 randPosition = new Vector3(Random.Range(-itemXSpread, itemXSpread), Random.Range(-itemYSpread, itemYSpread), Random.Range(-itemZSpread, itemZSpread)) + transform.position;
        ///// The `+ transform.position` makes it an offset from where the ItemAreaSpawner is located.
        itemXSpread = m_screenBoundary.x;
        itemYSpread = m_screenBoundary.y;
        Vector2 randPosition = new Vector2(Random.Range(m_screenBoundMin.x, m_screenBoundary.x), Random.Range(m_screenBoundMin.y, m_screenBoundary.y));

        GameObject clone = Instantiate(itemToSpread, randPosition, Quaternion.identity);
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube((m_screenBoundMin + m_screenBoundary)/2, m_screenBoundary - m_screenBoundMin);
    }

}

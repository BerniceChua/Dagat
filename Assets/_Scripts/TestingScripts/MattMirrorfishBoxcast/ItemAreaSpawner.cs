﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAreaSpawner : MonoBehaviour {

    public GameObject itemToSpread;
    public int numItemsToSpawn = 10;

    public float itemXSpread = 10.0f;
    public float itemYSpread = 0.0f;
    public float itemZSpread = 10.0f;

    // Start is called before the first frame update
    void Start() {
        //SpreadItem();

        for (int i = 0; i < numItemsToSpawn; i++) {
            SpreadItem();
        }
    }

    //// Update is called once per frame
    //void Update() {
        
    //}

    private void SpreadItem() {
        Vector3 randPosition = new Vector3(Random.Range(-itemXSpread, itemXSpread), Random.Range(-itemYSpread, itemYSpread), Random.Range(-itemZSpread, itemZSpread)) + transform.position;
        /// The `+ transform.position` makes it an offset from where the ItemAreaSpawner is located.
        GameObject clone = Instantiate(itemToSpread, randPosition, Quaternion.identity);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoOverlap2D : MonoBehaviour {
    public GameObject[] itemsToPickFrom;
    //public float raycastDistance = 100.0f;
    public float overlapTestBoxSize = 10.0f;
    public float m_OverlapTestBoxSizeMinimum = 1.0f;
    
    public LayerMask spawnedObjectLayer;
    
    /// <summary>
    /// Put this outside the function so it's not making a new array each time.
    /// </summary>
    private Collider2D[] collidersInsideOverlapBox = new Collider2D[1];

    private Vector2 m_screenBoundMin;
    private Vector2 m_screenBoundary;

    [SerializeField] private float m_randomYMin = -6;
    [SerializeField] private float m_randomYMax = 6;

    [SerializeField] private float m_randomOngoingXMin = -5;
    [SerializeField] private float m_randomOngoingXMax = 5;

    [SerializeField] public GameObject m_playerCharacter;
    private float m_xOffset;
    private float m_yOffset;

    Vector3 overlapTestBoxScale;
    Vector2 minV2;
    Vector2 maxV2;

    // Start is called before the first frame update
    void Start() {
        PositionRaycast();
    }

    // Update is called once per frame
    void Update() {
        
    }

    private void PositionRaycast() {
        //RaycastHit hit;

        //if (Physics.Raycast(transform.position, Vector3.down, out hit, raycastDistance)) {

        //    Quaternion spawnRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

        //    Vector3 overlapTestBoxScale = new Vector3(overlapTestBoxSize, overlapTestBoxSize, overlapTestBoxSize);
        //    /// Where line 11 originally was.
        //    //Collider[] collidersInsideOverlapBox = new Collider[1];
        //    int numberOfCollidersFound = Physics.OverlapBoxNonAlloc(hit.point, overlapTestBoxScale, collidersInsideOverlapBox, spawnRotation, spawnedObjectLayer);

        //    Debug.Log("Number Of Colliders Found = " + numberOfCollidersFound);

        //    if (numberOfCollidersFound == 0) {
        //        //Debug.Log("Spawned the game object");
        //        Pick(hit.point, spawnRotation);
        //    } else {
        //        Debug.Log("Name Of Collider 0 Found = " + collidersInsideOverlapBox[0].name);
        //    }
        //}

        m_screenBoundMin = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f - (Screen.height / 2), 0.0f));
        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 2, Screen.height + (Screen.height / 2), 0.0f));

        //float xVal = Random.Range(m_screenBoundary.x + m_randomOngoingXMin, m_screenBoundary.x + m_randomOngoingXMax);
        float xVal = Random.Range(m_screenBoundMin.x, m_screenBoundary.x);
        //Debug.Log("m_screenBoundary.x = " + m_screenBoundary.x);
        //Debug.Log("xVal = " + xVal);
        /// The minumum & maximum values came from 10.8 divided by 2.
        /// 10.8 came from the height of the intended play area.
        float yVal = Mathf.Clamp((Random.Range(m_playerCharacter.transform.position.y + m_yOffset + m_randomYMin, m_playerCharacter.transform.position.y + m_yOffset + m_randomYMax)), -52.0f, 52.0f);

        transform.position = new Vector2(xVal, yVal);

        overlapTestBoxScale = new Vector3(overlapTestBoxSize * 2.0f, overlapTestBoxSize * 1.5f, 0.0f);
        /// Where line 11 originally was.
        //Collider[] collidersInsideOverlapBox = new Collider[1];
        //int numberOfCollidersFound = Physics.OverlapBoxNonAlloc(hit.point, overlapTestBoxScale, collidersInsideOverlapBox, spawnRotation, spawnedObjectLayer);
        minV2 = this.gameObject.transform.position - overlapTestBoxScale;
        maxV2 = this.gameObject.transform.position + overlapTestBoxScale;
        int numberOf2DCollidersFound = Physics2D.OverlapAreaNonAlloc(minV2, maxV2, collidersInsideOverlapBox, spawnedObjectLayer);

        Debug.Log("Number Of 2D Colliders Found = " + numberOf2DCollidersFound);

        if (numberOf2DCollidersFound == 0) {
            //Debug.Log("Spawned the game object");
            Pick(transform.position, Quaternion.identity);
        } else {
            Debug.Log("Name Of Collider 0 Found = " + collidersInsideOverlapBox[0].name);
        }
    }

    private void Pick(Vector2 positionToSpawn, Quaternion rotationToSpawn) {
        Debug.Log("Inside Pick(Vector2 positionToSpawn, Quaternion rotationToSpawn)");
        int randomIndex = Random.Range(0, itemsToPickFrom.Length);
        GameObject clone = Instantiate(itemsToPickFrom[randomIndex], positionToSpawn, rotationToSpawn);
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireCube( (minV2 + maxV2)/2, overlapTestBoxScale * 2);
    }

    private float OverlapTestBoxSizeLimit() {
        if (overlapTestBoxSize < m_OverlapTestBoxSizeMinimum) {
            overlapTestBoxSize = m_OverlapTestBoxSizeMinimum;
        }
        return overlapTestBoxSize;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// https://answers.unity.com/questions/40996/check-if-position-is-inside-a-collider.html
/// </summary>
public class TestCreateHexTileMap3 : MonoBehaviour
{
    public GameObject hexPrefab;

    //[HideInInspector] public List<Vector2> m_PossibleV2Positions = new List<Vector2>();

    // Size of the map in terms of number of hex tiles
    // This is NOT representative of the amount of 
    // world space that we're going to take up.
    // (i.e. our tiles might be more or less than 1 Unity World Unit)
    [SerializeField] private int m_width = 4;
    [SerializeField] private int m_height = 3;

    //[SerializeField] private float xOffset = 0.882f;
    //[SerializeField] private float yOffset = 0.764f;
    [SerializeField] private float m_xOffset = 2.5f;
    [SerializeField] private float m_yOffset = 2.17f;
    [SerializeField] public float m_OffsetMultiplier = 5.0f;
    [SerializeField] public float m_OffsetShrinkRate = 0.2f;

    /// <summary>
    /// Internally sets the starting position based on the screen width & height.
    /// </summary>
    private float m_startXPos;
    private float m_startYPos;
    private Vector2 m_screenBoundary;
    private Vector2 m_screenBottomRight;
    private Vector2 m_screenBottomLeft;

    /// <summary>
    /// Connects this to the timer of the GameLoopManager.cs
    /// </summary>
    [SerializeField] private GameLoopManager m_timer;
    //private float m_timeInterval = 60000.0f;
    [SerializeField] private float m_timeInterval = 60.0f;

    [SerializeField] PlayerMovement m_playerCharacter;

    // Use this for initialization
    void Start()
    {

        //m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        //m_screenBottomRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, Camera.main.transform.position.z));
        //m_screenBottomLeft = Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z));

        //m_startXPos = m_screenBottomRight.x;
        //m_startYPos = m_screenBottomRight.y;

        //CreateHexTiles();


        List<Vector2> m_PossibleV2Positions = new List<Vector2>();

        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        m_screenBottomRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, Camera.main.transform.position.z));
        m_screenBottomLeft = Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z));

        m_startXPos = m_screenBottomRight.x;
        m_startYPos = m_screenBottomRight.y;


        for (int x = 0; x < m_width; x++)
        {
            for (int y = 0; y < m_height; y++)
            {

                //float xPos = x * m_xOffset;
                //float xPos = m_startXPos + (x * m_xOffset);
                //float xPos = m_startXPos;
                float xPos = (m_startXPos / 4) + (x * m_xOffset);
                float yPos = (m_startYPos / 2.3f) + (y * m_yOffset);

                // Are we on an odd row?
                if (y % 2 == 1)
                {
                    xPos += m_xOffset / 2f;
                }

                m_PossibleV2Positions.Add(new Vector2(xPos * m_OffsetMultiplier, yPos * m_OffsetMultiplier));

                GameObject hex_go = (GameObject)Instantiate(hexPrefab, new Vector3(xPos * m_OffsetMultiplier, yPos * m_OffsetMultiplier, 2.0f), Quaternion.identity);

                // Name the gameobject something sensible.
                hex_go.name = "Hex_" + x + "_" + y;

                // Make sure the hex is aware of its place on the map
                hex_go.GetComponent<Hex2D>().x = x;
                hex_go.GetComponent<Hex2D>().y = y;

                // For a cleaner hierachy, parent this hex to the map
                hex_go.transform.SetParent(this.transform);

            }
        }
        Debug.Log("m_PossibleV2Positions.Length = " + m_PossibleV2Positions.Count);
        //foreach (Vector2 v2Pos in m_PossibleV2Positions)
        //{
        //    Debug.Log(v2Pos);
        //}

        //InvokeRepeating("RecycleHazards", 8.0f, 8.0f);
        InvokeRepeating("CreateHexTiles", 0.0f, 2.0f);

        /// from ShrinkOffsetMultiplier()
        InvokeRepeating("ShrinkOffsetMultiplier", m_timeInterval, m_timeInterval);
        /// from IncreaseTilesWidthAndHeight()
        InvokeRepeating("IncreaseTilesWidthAndHeight", (m_timeInterval * 5), (m_timeInterval * 5));
    }

    //// Start is called before the first frame update
    //void Start()
    //{

    //}

    // Update is called once per frame
    void Update()
    {
        //if (m_timer.m_RunningTime >= m_timeInterval) {
        //    ShrinkOffsetMultiplier();

        //    if (m_timer.m_RunningTime >= (m_timeInterval*10)) {
        //        IncreaseTilesWidthAndHeight();
        //    }
        //}
    }

    private void LateUpdate()
    {
        //if (m_playerCharacter.transform.position.x > m_screenBoundary.x * 2)
        //{
        //    /// Recycle the hazards
        //    RecycleHazards();
        //}
    }

    public List<Vector2> CreateHexTiles() {

        List<Vector2> m_PossibleV2Positions = new List<Vector2>();

        m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        m_screenBottomRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, Camera.main.transform.position.z));
        m_screenBottomLeft = Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z));

        //Debug.Log("m_screenBottomRight = " + m_screenBottomRight);
        Debug.Log("m_screenBottomRight = " + m_screenBottomLeft);
        m_startXPos = m_screenBottomLeft.x;
        m_startYPos = m_screenBottomLeft.y;
        Debug.Log("m_startXPos = " + m_startXPos);
        Debug.Log("m_startYPos = " + m_startYPos);

        for (int x = 0; x < m_width; x++)
        {
            for (int y = 0; y < m_height; y++)
            {

                //float xPos = x * m_xOffset;
                //float xPos = m_startXPos + (x * m_xOffset);
                //float xPos = m_startXPos;
                float xPos = (m_startXPos) + (x * m_xOffset);
                float yPos = (m_startYPos / 2.3f) + (y * m_yOffset);
                Debug.Log(xPos + ", " + yPos);

                // Are we on an odd row?
                if (y % 2 == 1)
                {
                    xPos += m_xOffset / 2f;
                }

                m_PossibleV2Positions.Add(new Vector2(xPos * m_OffsetMultiplier, yPos * m_OffsetMultiplier));

                GameObject hex_go = (GameObject)Instantiate(hexPrefab, new Vector3(xPos * m_OffsetMultiplier, yPos * m_OffsetMultiplier, 2.0f), Quaternion.identity);

                // Name the gameobject something sensible.
                hex_go.name = "Hex_" + x + "_" + y;

                // Make sure the hex is aware of its place on the map
                hex_go.GetComponent<Hex2D>().x = x;
                hex_go.GetComponent<Hex2D>().y = y;

                // For a cleaner hierachy, parent this hex to the map
                hex_go.transform.SetParent(this.transform);

            }
        }
        Debug.Log("m_PossibleV2Positions.Length = " + m_PossibleV2Positions.Count);
        //foreach (Vector2 v2Pos in m_PossibleV2Positions)
        //{
        //    Debug.Log(v2Pos);
        //}
        return m_PossibleV2Positions;
    }

    private void ShrinkOffsetMultiplier() {
        Debug.Log("Shrink!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        //m_OffsetMultiplier = m_OffsetMultiplier * (1.0f - m_OffsetShrinkRate);
        m_OffsetMultiplier -= m_OffsetShrinkRate;
    }

    private void IncreaseTilesWidthAndHeight() {
        Debug.Log("Increase!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        m_width++;
        m_height++;
    }

    //public void RecycleHazards()
    //{
    //    m_screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    //    m_screenBottomRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, Camera.main.transform.position.z));
    //    m_screenBottomLeft = Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z));

    //    m_startXPos = m_screenBottomRight.x;
    //    m_startYPos = m_screenBottomRight.y;

    //    Debug.Log("m_screenBottomRight = " + m_screenBottomRight);

    //    CreateHexTiles();
    //}
}

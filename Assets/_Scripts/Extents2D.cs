﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Extents2D {

    public float upExtent;
    public float downExtent;
    public float leftExtent;
    public float rightExtent;

    public Extents2D Find2DExtents(Collider2D collider2D) {
        Extents2D the2DExtents = new Extents2D();

        Vector2 centerPoint = collider2D.bounds.center;
        float width = collider2D.bounds.extents.x;
        float height = collider2D.bounds.extents.y;

        the2DExtents.upExtent = centerPoint.y + height;
        the2DExtents.downExtent = centerPoint.y - height;
        the2DExtents.leftExtent = centerPoint.x - width;
        the2DExtents.rightExtent = centerPoint.x + width;

        return the2DExtents;
    }

}

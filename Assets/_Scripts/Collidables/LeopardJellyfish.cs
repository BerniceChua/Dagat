﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeopardJellyfish : MonoBehaviour, ICollidable {
    public string CollideMessage() {
        return "Stung by a Leopard Jellyfish.";
    }

    public bool Enabled {
        get { return enabled; }
        set { enabled = value; }
    }

}

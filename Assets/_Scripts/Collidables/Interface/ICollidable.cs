﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICollidable {
    string CollideMessage();

    bool Enabled { get; set; }

}

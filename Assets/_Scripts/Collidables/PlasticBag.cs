﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasticBag : MonoBehaviour, ICollidable {
    public string CollideMessage() {
        return "Suffocated by a plastic bag.";
    }

    public bool Enabled {
        get { return enabled; }
        set { enabled = value; }
    }

}

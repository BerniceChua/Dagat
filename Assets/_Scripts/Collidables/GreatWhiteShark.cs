﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreatWhiteShark : MonoBehaviour, ICollidable {
    public string CollideMessage() {
        return "Eaten by a Great White Shark.";
    }

    public bool Enabled {
        get { return enabled; }
        set { enabled = value; }
    }

}

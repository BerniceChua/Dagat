﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Backgrounds : MonoBehaviour {

    //private BackgroundsManager m_backgroundsManager;
    [SerializeField] private BackgroundsManager m_backgroundsManager;

    private void OnEnable() {
        //m_backgroundsManager = GameObject.FindObjectOfType<BackgroundsManager>();
    }

    private void OnBecameInvisible() {
        /// Recycle the backgrounds
        m_backgroundsManager.RecycleBackgrounds(this.gameObject);
    }

}

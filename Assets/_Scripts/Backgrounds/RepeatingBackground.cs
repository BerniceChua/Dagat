﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackground : MonoBehaviour {

    private BoxCollider2D m_backgroundCollider;
    private float m_backgroundWidth;
    private float m_backgroundHeight;

    [SerializeField] private GameObject[] m_backgrounds;
    private int m_numberOfBackgrounds;

    [SerializeField] private GameObject m_playerCharacter;

    private void Awake() {
        m_numberOfBackgrounds = m_backgrounds.Length;
    }

    // Start is called before the first frame update
    void Start()
    {
        m_backgroundCollider = GetComponent<BoxCollider2D>();
        m_backgroundWidth = m_backgroundCollider.size.x;
        m_backgroundHeight = m_backgroundCollider.size.y;
    }

    // Update is called once per frame
    void Update() {
        if (transform.position.y < -(m_backgroundHeight + m_playerCharacter.transform.position.y) ) {
            RepeatBackgroundHeightUp();
        }

        if (transform.position.y > (m_backgroundHeight + m_playerCharacter.transform.position.y) ) {
            RepeatBackgroundHeightDown();
        }

        if (transform.position.x < -(m_backgroundWidth + m_playerCharacter.transform.position.x) ) {
            RepeatBackgroundWidth();
        }
    }

    private void RepeatBackgroundHeightUp() {
        Vector2 backgroundOffset = new Vector2(0.0f, m_backgroundHeight * m_numberOfBackgrounds * this.transform.localScale.y);
        transform.position = (Vector2)transform.position + backgroundOffset;
    }

    private void RepeatBackgroundHeightDown() {
        Vector2 backgroundOffset = new Vector2(0.0f, m_backgroundHeight * m_numberOfBackgrounds * this.transform.localScale.y);
        transform.position = -((Vector2)transform.position + backgroundOffset);
    }

    private void RepeatBackgroundWidth() {
        Vector2 backgroundOffset = new Vector2(m_backgroundWidth * m_numberOfBackgrounds * this.transform.localScale.x, 0.0f);
        transform.position = (Vector2)transform.position + backgroundOffset;
    }

}

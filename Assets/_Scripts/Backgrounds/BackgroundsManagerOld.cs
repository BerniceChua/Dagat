﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundsManagerOld : MonoBehaviour {

    [SerializeField] private GameObject[] m_backgroundsTiles;

    [SerializeField] private GameObject m_playerCharacter;
    private Vector2 m_initialPlayerPosition;
    private Vector2 m_currentPlayerPosition;

    private float m_xOffset;
    private float m_yOffset;

    //private float m_backgroundWidth = 57.6f;
    //private float m_backgroundHeight = 16.2f;
    private float m_backgroundWidth;
    private float m_backgroundHeight;
    private float m_noChange = 0.0f;

    // Start is called before the first frame update
    void Start() {
        m_initialPlayerPosition = m_playerCharacter.transform.position;

        m_backgroundWidth = m_backgroundsTiles[0].GetComponent<BoxCollider2D>().size.x;
        m_backgroundHeight = m_backgroundsTiles[0].GetComponent<BoxCollider2D>().size.y;

        m_xOffset = m_backgroundWidth * 2.0f;
        m_yOffset = m_backgroundHeight * 2.0f;
    }

    // Update is called once per frame
    void Update() {
        //m_xOffset = m_playerCharacter.transform.position.x;
        //m_yOffset = m_playerCharacter.transform.position.y;
        //m_currentPlayerPosition = m_playerCharacter.transform.position;
    }

    public void RecycleBackgrounds(GameObject background) {
        Debug.Log("Recycling " + background.name);

        /// Reposition to the next offset.
        //background.transform.position = new Vector3(m_xOffset, m_yOffset);
        background.transform.position = new Vector2(m_xOffset, 0.0f);

        //if (m_currentPlayerPosition.x % (m_backgroundWidth * 2) > 0) {
        //    m_xOffset += m_backgroundWidth;
        //} else {
        //    m_xOffset = m_noChange;
        //}

        //if ( (m_currentPlayerPosition.x > m_initialPlayerPosition.x) && (m_currentPlayerPosition.x % (m_backgroundWidth) == 0) ) {
        //    //m_xOffset += m_backgroundWidth + m_currentPlayerPosition.x;
        //    m_xOffset += m_backgroundWidth;
        //    CheckIfNeedToChangeYOffset();
        //} else {
        //    m_xOffset += m_noChange;
        //    CheckIfNeedToChangeYOffset();
        //}

        m_xOffset += m_backgroundWidth;
        CheckIfNeedToChangeYOffset();

        m_initialPlayerPosition = m_currentPlayerPosition;
        m_currentPlayerPosition = m_playerCharacter.transform.position;
        Debug.Log("m_initialPlayerPosition = " + m_initialPlayerPosition);
        Debug.Log("m_currentPlayerPosition = " + m_currentPlayerPosition);
    }

    private void CheckIfNeedToChangeYOffset() {
        Debug.Log("need the Y offset at " + m_currentPlayerPosition.y);

        //if (m_currentPlayerPosition.y > (m_initialPlayerPosition.y) ) {
        //    m_yOffset += m_backgroundHeight;
        //} else if (m_currentPlayerPosition.y < (m_initialPlayerPosition.y) ) {
        //    m_yOffset -= m_backgroundHeight;
        //} else {
        //    m_yOffset = m_noChange;
        //}

        //if (m_currentPlayerPosition.y % (m_backgroundHeight * 2) > 0) {
        //    Debug.Log("inside if (m_currentPlayerPosition.y % (m_backgroundHeight * 2) > 0)");
        //    Debug.Log(m_currentPlayerPosition.y % (m_backgroundHeight * 2));
        //    m_yOffset += m_backgroundHeight;
        //} else if (m_currentPlayerPosition.y % (m_backgroundHeight * 2) < 0) {
        //    Debug.Log("inside else if (m_currentPlayerPosition.y % (m_backgroundHeight * 2) < 0)");
        //    Debug.Log(m_currentPlayerPosition.y % (m_backgroundHeight * 2));
        //    m_yOffset -= m_backgroundHeight;
        //} else {
        //    m_yOffset = m_noChange;
        //}

        //if ( (m_currentPlayerPosition.y % m_backgroundHeight == 0) && (m_currentPlayerPosition.y > m_initialPlayerPosition.y) ) {
        //    Debug.Log("inside if (m_currentPlayerPosition.y % (m_backgroundHeight * 2) == 0)");
        //    Debug.Log(m_currentPlayerPosition.y % (m_backgroundHeight));
        //    m_yOffset += m_backgroundHeight + m_currentPlayerPosition.y;
        //} else if ( (m_currentPlayerPosition.y % (m_backgroundHeight) == 0) && (m_currentPlayerPosition.y < m_initialPlayerPosition.y) ) {
        //    Debug.Log("inside else if (m_currentPlayerPosition.y % (m_backgroundHeight) == 0)");
        //    Debug.Log(m_currentPlayerPosition.y % (m_backgroundHeight));
        //    m_yOffset -= (m_backgroundHeight + m_currentPlayerPosition.y);
        //} else {
        //    m_yOffset += m_noChange;
        //}

        //if ( (m_currentPlayerPosition.y > m_initialPlayerPosition.y) && (m_currentPlayerPosition.y % m_backgroundHeight == 0) ) {
        //    Debug.Log("inside if (m_currentPlayerPosition.y % (m_backgroundHeight * 2) == 0)");
        //    Debug.Log(m_currentPlayerPosition.y % (m_backgroundHeight));
        //    m_yOffset += m_backgroundHeight;
        //} else if ( (m_currentPlayerPosition.y < m_initialPlayerPosition.y) && (m_currentPlayerPosition.y % (m_backgroundHeight) == 0) ) {
        //    Debug.Log("inside else if (m_currentPlayerPosition.y % (m_backgroundHeight) == 0)");
        //    Debug.Log(m_currentPlayerPosition.y % (m_backgroundHeight));
        //    m_yOffset -= (m_backgroundHeight);
        //} else {
        //    m_yOffset += m_noChange;
        //}

        if ((m_currentPlayerPosition.y > m_initialPlayerPosition.y) && (m_currentPlayerPosition.y % m_backgroundHeight == 0)) {
            Debug.Log("inside if (m_currentPlayerPosition.y % (m_backgroundHeight * 2) == 0)");
            Debug.Log(m_currentPlayerPosition.y % (m_backgroundHeight));
            m_yOffset += m_backgroundHeight + m_currentPlayerPosition.y;
        } else if ((m_currentPlayerPosition.y < m_initialPlayerPosition.y) && (m_currentPlayerPosition.y % (m_backgroundHeight) == 0)) {
            Debug.Log("inside else if (m_currentPlayerPosition.y % (m_backgroundHeight) == 0)");
            Debug.Log(m_currentPlayerPosition.y % (m_backgroundHeight));
            m_yOffset -= (m_backgroundHeight + m_currentPlayerPosition.y);
        } else {
            m_yOffset += m_noChange;
        }
    }

}

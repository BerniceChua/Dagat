﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Source: 
/// https://www.youtube.com/watch?v=WYD9iq8ku_w&list=PLadYLGMfR6Lo_XLHO1nQmYhXGrQV2pEAM&index=2
/// </summary>
public class BackgroundsManager : MonoBehaviour {

    [SerializeField] private GameObject[] m_backgroundsTiles;

    //[SerializeField] private GameObject m_playerCharacter;
    //private Vector2 m_initialPlayerPosition;
    //private Vector2 m_currentPlayerPosition;

    private float m_xOffset;
    private float m_yOffset;

    private float m_backgroundWidth;
    private float m_backgroundHeight;
    private float m_noChange = 0.0f;

    // Start is called before the first frame update
    void Start() {

        //m_initialPlayerPosition = m_playerCharacter.transform.position;

        m_backgroundWidth = m_backgroundsTiles[0].GetComponent<BoxCollider2D>().size.x;
        m_backgroundHeight = m_backgroundsTiles[0].GetComponent<BoxCollider2D>().size.y;

        m_xOffset = m_backgroundWidth * 2.0f;
        m_yOffset = m_backgroundHeight * 2.0f;
    }

    public void RecycleBackgrounds(GameObject background) {
        //Debug.Log("Recycling " + background.name);

        /// Reposition to the next offset.
        //background.transform.position = new Vector3(m_xOffset, m_yOffset);
        background.transform.position = new Vector2(m_xOffset, 0.0f);

        m_xOffset += m_backgroundWidth;

        //m_initialPlayerPosition = m_currentPlayerPosition;
        //m_currentPlayerPosition = m_playerCharacter.transform.position;
        //Debug.Log("m_initialPlayerPosition = " + m_initialPlayerPosition);
        //Debug.Log("m_currentPlayerPosition = " + m_currentPlayerPosition);
    }

}
